-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `dodavatel`;
CREATE TABLE `dodavatel` (
  `dodavatel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazov` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `cislo` varchar(15) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `doprava` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `poznamka` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `smazano` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`dodavatel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `dodavatel` (`dodavatel_id`, `nazov`, `cislo`, `email`, `doprava`, `poznamka`, `smazano`) VALUES
(1,	'KOH-I-NOOR a.s.',	'26055996',	'sales@koh-i-noor.cz',	'poštou',	'',	0),
(2,	'CRETACOLOR',	'47524383',	'info@cretacolor.com',	'pošta, kurier',	'',	0),
(3,	'Royal & Langnickel',	'90562313',	'info@roylang.com',	'kurier',	'',	0);

DROP TABLE IF EXISTS `dodavatel_objednavka`;
CREATE TABLE `dodavatel_objednavka` (
  `dodavatel_objednavka_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dodavatel_id` int(10) unsigned NOT NULL,
  `sortiment_id` int(10) unsigned NOT NULL,
  `kusy` int(10) unsigned NOT NULL,
  `datum_doruceni` date DEFAULT NULL,
  `datum_vytvoreni` date DEFAULT NULL,
  `celkem` double unsigned NOT NULL,
  `stav` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`dodavatel_objednavka_id`),
  KEY `dodavatel_id` (`dodavatel_id`),
  KEY `sortiment_id` (`sortiment_id`),
  CONSTRAINT `dodavatel_objednavka_ibfk_1` FOREIGN KEY (`dodavatel_id`) REFERENCES `dodavatel` (`dodavatel_id`) ON DELETE CASCADE,
  CONSTRAINT `dodavatel_objednavka_ibfk_2` FOREIGN KEY (`sortiment_id`) REFERENCES `sortiment` (`sortiment_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `doprava`;
CREATE TABLE `doprava` (
  `doprava_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nazov` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `cena` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`doprava_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `doprava` (`doprava_id`, `nazov`, `cena`) VALUES
(1,	'Česká pošta',	25),
(2,	'PPL',	0),
(3,	'DPD',	10),
(4,	'Česká pošta - balík do ruky',	20);

DROP TABLE IF EXISTS `farbicky`;
CREATE TABLE `farbicky` (
  `sortiment_id` int(10) unsigned NOT NULL,
  `dlzka` int(11) NOT NULL,
  `vlastnost` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`sortiment_id`),
  CONSTRAINT `farbicky_ibfk_2` FOREIGN KEY (`sortiment_id`) REFERENCES `sortiment` (`sortiment_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `farbicky` (`sortiment_id`, `dlzka`, `vlastnost`) VALUES
(1,	175,	'školské, 6-hranné'),
(2,	175,	'školské, 6-hranné'),
(3,	180,	'akvarelové, odolné voči svetlu'),
(4,	175,	'školské, 6-hranné'),
(5,	180,	'akvarelové, odolné voči svetlu'),
(6,	180,	'akvarelové, odolné voči svetlu'),
(7,	180,	'akvarelové, odolné voči svetlu'),
(8,	180,	'akvarelové, odolné voči svetlu'),
(9,	175,	'školské, 6-hranné');

DROP TABLE IF EXISTS `kategorie`;
CREATE TABLE `kategorie` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `smazano` int(1) unsigned DEFAULT '0',
  PRIMARY KEY (`category_id`),
  KEY `parent_id` (`parent_id`),
  CONSTRAINT `kategorie_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `kategorie` (`category_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `kategorie` (`category_id`, `name`, `parent_id`, `smazano`) VALUES
(1,	'Farbičky',	NULL,	0),
(2,	'Skicaky',	NULL,	0);

DROP TABLE IF EXISTS `objednavka`;
CREATE TABLE `objednavka` (
  `objednavka_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `zakaznik_id` int(10) unsigned DEFAULT NULL,
  `zamestnanec_id` int(10) unsigned DEFAULT NULL,
  `doprava_id` int(10) unsigned NOT NULL,
  `datum` datetime NOT NULL,
  `stav` int(11) NOT NULL DEFAULT '0',
  `poznamka` text COLLATE utf8_czech_ci NOT NULL,
  `mesto` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `psc` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `ulice` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `cislo_popisne` varchar(10) COLLATE utf8_czech_ci NOT NULL,
  `string_id` varchar(124) COLLATE utf8_czech_ci NOT NULL,
  `datum_vytvoreni` datetime NOT NULL,
  PRIMARY KEY (`objednavka_id`),
  KEY `zakaznik_id` (`zakaznik_id`),
  KEY `zamestnanec_id` (`zamestnanec_id`),
  KEY `doprava_id` (`doprava_id`),
  CONSTRAINT `objednavka_ibfk_7` FOREIGN KEY (`doprava_id`) REFERENCES `doprava` (`doprava_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `objednavka_ibfk_3` FOREIGN KEY (`zakaznik_id`) REFERENCES `zakaznik` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `objednavka_ibfk_4` FOREIGN KEY (`zamestnanec_id`) REFERENCES `zamestnanec` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `objednavka_sortiment`;
CREATE TABLE `objednavka_sortiment` (
  `objednavka_sortiment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sortiment_id` int(10) unsigned NOT NULL,
  `objednavka_id` int(10) unsigned NOT NULL,
  `mnozstvo` int(11) unsigned NOT NULL,
  PRIMARY KEY (`objednavka_sortiment_id`),
  KEY `sortiment_id` (`sortiment_id`),
  KEY `objednavka_id` (`objednavka_id`),
  CONSTRAINT `objednavka_sortiment_ibfk_5` FOREIGN KEY (`objednavka_id`) REFERENCES `objednavka` (`objednavka_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `objednavka_sortiment_ibfk_3` FOREIGN KEY (`sortiment_id`) REFERENCES `sortiment` (`sortiment_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `page`;
CREATE TABLE `page` (
  `url` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `title` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `content` text COLLATE utf8_czech_ci,
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `page` (`url`, `title`, `content`) VALUES
('domov',	'Domov',	'<section>\n<p><strong>Vitajte na na&scaron;ej str&aacute;nke.</strong></p>\n<div class=\"cistic\">&nbsp;</div>\n</section>'),
('kontakty',	'Kontakty',	'<section>\n<p><strong>V pr&iacute;pade ak&yacute;chkoľvek ot&aacute;zok alebo pripomienok n&aacute;s nev&aacute;hajte kontaktovať!</strong></p>\n<p>&nbsp;</p>\n<p><strong>Email: &nbsp; </strong>info@cernyblaskova.com</p>\n<p><strong>Tel.:&nbsp;&nbsp; </strong>+420 900 321 321</p>\n<p>&nbsp;</p>\n<p><strong>Adresa:</strong></p>\n<p>Cerny&amp;Blaskova, a.s.</p>\n<p>Kolejn&iacute; 2</p>\n<p>612 00, Brno</p>\n<p>&nbsp;</p>\n<p><strong>IČO:&nbsp;</strong> 00321321</p>\n<p><strong>DIČ: </strong>CZ00321321</p>\n</section>'),
('o_nas',	'O nás',	'<section>\n<p>Spoločnosť Cerny&amp;Blaskova je nov&aacute;čikom na trhu. To ale neznamen&aacute;, že zaost&aacute;va oproti in&yacute;mi. Pr&aacute;ve naopak, zamerali sme sa na Farbičky a Skic&aacute;ky, a t&yacute;m prin&aacute;&scaron;ame kvalitu a pestrosť sortimentu za svel&eacute; ceny!</p>\n<p>Každ&yacute; si u n&aacute;s n&aacute;jde, čo potrebuje, od žiačikov, amat&eacute;rov až po profesion&aacute;lnych umelcov s n&aacute;ročn&yacute;mi požiadavkami.Tak nev&aacute;hajte a pozrite si na&scaron;u ponuku produktov. Ak by ste mali ak&eacute;koľvek ot&aacute;zky alebo pripomienky nev&aacute;hajte n&aacute;s kontaktovať! N&aacute;&scaron;e &uacute;daje &uacute;daje n&aacute;jdete v sekci&iacute; Kontakty.</p>\n<p>Prajeme pekn&yacute; deň. :)</p>\n<p>Cerny&amp;Blaskova</p>\n</section>');

DROP TABLE IF EXISTS `recenzia`;
CREATE TABLE `recenzia` (
  `reenzia_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `sortiment_id` int(10) unsigned NOT NULL,
  `datum` datetime NOT NULL,
  `obsah` text COLLATE utf8_czech_ci NOT NULL,
  `hodnotenie` int(1) NOT NULL,
  PRIMARY KEY (`reenzia_id`),
  KEY `zakaznik_id` (`user_id`),
  KEY `sortiment_id` (`sortiment_id`),
  CONSTRAINT `recenzia_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `zakaznik` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `recenzia_ibfk_5` FOREIGN KEY (`sortiment_id`) REFERENCES `sortiment` (`sortiment_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `skicaky`;
CREATE TABLE `skicaky` (
  `sortiment_id` int(10) unsigned NOT NULL,
  `gramaz` int(11) NOT NULL,
  `velikst` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`sortiment_id`),
  CONSTRAINT `skicaky_ibfk_2` FOREIGN KEY (`sortiment_id`) REFERENCES `sortiment` (`sortiment_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `skicaky` (`sortiment_id`, `gramaz`, `velikst`) VALUES
(10,	110,	'A3'),
(11,	250,	'A4'),
(12,	110,	'A4'),
(13,	120,	'A4');

DROP TABLE IF EXISTS `sortiment`;
CREATE TABLE `sortiment` (
  `sortiment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dodavatel_id` int(10) unsigned NOT NULL,
  `kategorie_id` int(10) unsigned NOT NULL,
  `pocet` int(10) unsigned NOT NULL,
  `typ` varchar(16) COLLATE utf8_czech_ci NOT NULL,
  `nazov` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `dostupnost` date DEFAULT NULL,
  `farba` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `cena` double unsigned NOT NULL,
  `smazano` int(1) unsigned NOT NULL DEFAULT '0',
  `pridano` date NOT NULL,
  `icon` varchar(32) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`sortiment_id`),
  KEY `dodavatel_id` (`dodavatel_id`),
  KEY `kategorie_id` (`kategorie_id`),
  CONSTRAINT `sortiment_ibfk_2` FOREIGN KEY (`dodavatel_id`) REFERENCES `dodavatel` (`dodavatel_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `sortiment_ibfk_4` FOREIGN KEY (`kategorie_id`) REFERENCES `kategorie` (`category_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `sortiment` (`sortiment_id`, `dodavatel_id`, `kategorie_id`, `pocet`, `typ`, `nazov`, `dostupnost`, `farba`, `cena`, `smazano`, `pridano`, `icon`) VALUES
(1,	1,	1,	10,	'školské farbičky',	'Farbičky SET 18ks',	'1901-12-27',	'18 farieb',	45,	1,	'2016-11-27',	'/tmp/phpRDkOm3'),
(2,	1,	1,	10,	'školské farbičky',	'Farbičky SET 18ks',	'1901-12-27',	'18 farieb',	45,	0,	'2016-11-27',	'ukw53eit5q.jpg'),
(3,	2,	1,	6,	'akvarelové cerus',	'Akvarelky SET 12ks',	'1901-12-27',	'12 farieb',	395,	0,	'2016-11-27',	't1xg8q115y.jpg'),
(4,	1,	1,	6,	'školské farbičky',	'Farbičky SET 6ks',	'1901-12-27',	'6 farieb',	16,	0,	'2016-11-27',	'57frjhwle5.jpg'),
(5,	2,	1,	0,	'akvarelové cerus',	'Akvarelka - WHITE',	'1901-12-27',	'biela',	53,	0,	'2016-11-27',	'bbnfrnivvd.jpg'),
(6,	2,	1,	2,	'akvarelové cerus',	'Akvarelka - BLACK',	'1901-12-27',	'čierna',	53,	0,	'2016-11-27',	'm7bx5ntjlw.jpg'),
(7,	2,	1,	50,	'akvarelové cerus',	'Akvarelka - RED',	'2017-02-03',	'červená',	53,	0,	'2016-11-27',	'9zgcwtupl3.jpg'),
(8,	2,	1,	11,	'akvarelové cerus',	'Akvarelka - YELLOW',	'1901-12-27',	'žltá',	53,	0,	'2016-11-27',	'o4wvben7j9.jpg'),
(9,	1,	1,	2,	'školské farbičky',	'Farbičky SET 12ks',	'2016-12-04',	'12 farieb',	32,	0,	'2016-11-27',	'w3w5hbsl8w.jpg'),
(10,	3,	2,	16,	'skicovací blok -',	'Skicák s textúrou 10list',	'1901-12-27',	'biela',	636,	0,	'2016-11-27',	'kgn5bzksha.jpg'),
(11,	1,	2,	0,	'skicovací blok -',	'Skicák POP AQUARELL 30lis',	'2017-02-07',	'biela',	159,	0,	'2016-11-27',	'6m77ji3tul.jpg'),
(12,	1,	2,	6,	'skicovací blok -',	'Skicák POP SKETCH 20lis',	'1901-12-27',	'biela',	53,	0,	'2016-11-27',	'ynd5tqykqk.jpg'),
(13,	3,	2,	3,	'skicovací blok -',	'Skicák s textúrou 5list',	'1901-12-27',	'béžová',	450,	0,	'2016-11-27',	'xxr4o4rm4n.jpg');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_czech_ci DEFAULT NULL,
  `role` int(1) NOT NULL DEFAULT '3',
  `smazano` int(1) NOT NULL DEFAULT '0',
  `registrovan` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `user` (`user_id`, `email`, `password`, `role`, `smazano`, `registrovan`) VALUES
(1,	'admin@farbicky-skicaky.cekuj.net',	'$2y$10$3BiSYCmAMC.mPZUCMfPIjeOBK83QE2AWydJkZ0t205FXEoivYMUvO',	0,	0,	1),
(2,	'zamestnanec@farbicky-skicaky.cekuj.net',	'$2y$10$3BiSYCmAMC.mPZUCMfPIjeOBK83QE2AWydJkZ0t205FXEoivYMUvO',	1,	0,	1),
(14,	'zakaznik@seznam.cz',	'$2y$10$3BiSYCmAMC.mPZUCMfPIjeOBK83QE2AWydJkZ0t205FXEoivYMUvO',	2,	0,	1);

DROP TABLE IF EXISTS `zakaznik`;
CREATE TABLE `zakaznik` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `priezvisko` varchar(64) COLLATE utf8_czech_ci NOT NULL,
  `cislo` varchar(15) COLLATE utf8_czech_ci NOT NULL,
  `registracia` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `zakaznik_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `zakaznik` (`user_id`, `priezvisko`, `cislo`, `registracia`) VALUES
(14,	'Lukáš Černý',	'+420728504659',	'2016-12-04 21:15:43');

DROP TABLE IF EXISTS `zamestnanec`;
CREATE TABLE `zamestnanec` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meno` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `priezvisko` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `rodne_cislo` varchar(12) COLLATE utf8_czech_ci NOT NULL,
  `pozicia` varchar(32) COLLATE utf8_czech_ci NOT NULL,
  `opravnenie` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  CONSTRAINT `zamestnanec_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `zamestnanec` (`user_id`, `meno`, `priezvisko`, `rodne_cislo`, `pozicia`, `opravnenie`) VALUES
(1,	'Hlavni',	'Admin',	'1122334444',	'Správce',	0),
(2,	'Ondřej',	'Čech',	'1234567890',	'Skladník',	0);

-- 2016-12-05 15:35:09
