<?php

namespace App\Services;

use Nette;

class ShoppingCartService
{

    /** @var \Nette\Http\Session */
    private $session;

    /** @var \Nette\Http\SessionSection */
    private $sessionSection;

    public function __construct(\Nette\Http\Session $session)
    {
        $this->session = $session;

        $this->sessionSection = $this->session->getSection('shopping_cart');
    }

    public function getShoppingList()
    {
        $result = [];
        if (count($this->sessionSection->shoppingList)) {
            $result = (array) $this->sessionSection->shoppingList;
        }
        return ($result);
    }

    public function updateShoppingList($id, $count)
    {
        $list = (array) $this->sessionSection->shoppingList;
        if (array_key_exists($id, $list)) {
            $list[$id] += $count;
        } else {
            $list[$id] = $count;
        }
        $this->sessionSection->shoppingList = $list;
    }

    public function removeShoppingList($id, $count = 0)
    {
        $list = (array) $this->sessionSection->shoppingList;
        if (array_key_exists($id, $list)) {
            $item = $list[$id];
            if ($item == $count || $count == 0) {
                unset($list[$id]);
            } else {
                $list[$id] = $count;
            }
            $this->sessionSection->shoppingList = $list;
        }
    }
    
    public function deleteShoppingList()
    {
        $this->sessionSection->shoppingList = [];
    }

}
