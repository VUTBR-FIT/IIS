<?php

namespace App\SystemModule\Presenters;

use \Nette\Application\UI\Form as Form;
use Joseki\Application\Responses\PdfResponse as Pdf;

/**
 * Description of OrderPresenter
 *
 * @author user
 */
class OrderPresenter extends BasePresenter
{

    /** @var \App\Model\Doprava @inject */
    public $transport;

    /** @var \App\Model\Zakaznik @inject */
    public $customer;

    /** @var \App\Model\Objednavka @inject */
    public $order;

    /** @var \App\Model\ObjednavkaSortiment @inject */
    public $orderSortiment;

    /** @var Array */
    private $arrTransport;

    /** @var Array */
    private $arrOrder;

    /** @var Array */
    private $arrSortiment;

    /** @var Array */
    private $arrUser;

    public function actionDefault()
    {
        $cart = (array) $this->shoppingCart->getShoppingList();

        if (!count($cart)) {
            $this->flashMessage('Nemůžete objednávat, protože máte prázdný košík');
            $this->redirect('Cart:default');
        }

        $data = $this->transport->getAll();

        foreach ($data as $value) {
            $this->arrTransport[$value->doprava_id] = $value->nazov . " (Cena: " . $value->cena . "Kč)";
        }
    }

    public function actionFinished($id)
    {
        if ($id == null) {
            $this->redirect('Homepage:default', ['id' => 'domov']);
        }

        $this->arrOrder = $this->order->getByStringId($id)->fetch();

        if ($this->arrOrder === false) {
            $this->flashMessage('Objednávka nebyla nalezena');
            $this->redirect('Homepage:default', ['id' => 'domov']);
        }
    }

    public function actionOrder($id)
    {
        if ($id == null) {
            $this->redirect('Homepage:default', ['id' => 'domov']);
        }

        $this->arrOrder = $this->order->getByStringId($id)->fetch();

        if ($this->arrOrder['stav'] != 1) {
            $this->flashMessage('Objednávka byla zrušena nebo není ještě vyřízena');
            $this->redirect('Order:finished', ['id' => $id]);
        }

        if ($this->arrOrder === false) {
            $this->flashMessage('Objednávka nebyla nalezena');
            $this->redirect('Homepage:default', ['id' => 'domov']);
        }

        $this->arrSortiment = $this->orderSortiment->getByOrderId($this->arrOrder)->fetchAll();

        $this->arrTransport = $this->transport->getAll()->fetchAll();

        $this->arrUser = $this->customer->getById($this->arrOrder->zakaznik_id)->fetch();

        $template = $this->createTemplate();
        $template->setFile(__DIR__ . '/templates/Order/order.latte');

        $template->order = $this->arrOrder;
        $template->transport = $this->arrTransport;
        $template->products = $this->arrSortiment;
        $template->arrUser = $this->arrUser;

        $pdf = new Pdf($template);

        $pdf->documentTitle = "Faktura " . $this->arrOrder->objednavka_id . $this->arrOrder->datum_vytvoreni->format('dmY');
        $pdf->documentAuthor = "Cerny & Blaskova";
        $pdf->pageFormat = 'A4-P';
        $pdf->setSaveMode(Pdf::INLINE);
        $this->sendResponse($pdf);
    }

    public function actionList()
    {
        if (!$this->getUser()->isLoggedIn()) {
            $this->redirect('SignIn:default');
        }
        $this->arrOrder = $this->order->getAll($this->getUser()->getId())->fetchAll();
    }

    public function renderFinished()
    {
        $this->template->order = $this->arrOrder;
    }

    public function renderOrder()
    {
        $this->template->order = $this->arrOrder;
        $this->template->transport = $this->arrTransport;
        $this->template->products = $this->arrSortiment;
        $this->template->arrUser = $this->arrUser;
    }

    public function renderList()
    {
        $this->template->list = $this->arrOrder;
    }

    protected function createComponentOrderForm()
    {
        $form = new Form;

        $form->addText('mesto')
                ->setRequired('Prosím zadejte místo doručení');
        $form->addText('psc')
                ->addRule(Form::PATTERN, "Prosím zkontrolujte PSČ", "[0-9]{5,}")
                ->setRequired('Prosím zadejte PSČ');
        $form->addText('ulice')
                ->setRequired('Prosím zadejte ulici');
        $form->addText('cislo_popisne')
                ->addRule(Form::PATTERN, "Prosím opravte číslo popisné", "[0-9a-zA-Z]+")
                ->setRequired('Prosím zadejte číslo popisné');
        $form->addTextArea('poznamka')
                ->setRequired(false);
        $form->addSelect('doprava_id', NULL, $this->arrTransport)
                ->setRequired('Prosím vyberte způsob dopravy');

        if (!$this->getUser()->isLoggedIn()) {
            $form->addEmail('email')
                    ->setRequired('Prosím zadejte email');
            $form->addText('priezvisko')
                    ->setRequired('Prosím zadejte jméno a příjmení');
            $form->addText('cislo')
                    ->setRequired('Prosím zadejte telefonní číslo')
                    ->setAttribute('type', 'phone');
        }

        $form->addSubmit('send');

        $form->onError[] = array($this, 'formError');
        $form->onSuccess[] = [$this, 'formOrder'];

        return ($form);
    }

    public function formOrder(Form $form)
    {
        $values = $form->getValues();
        $id = null;
        try {
            if (!$this->getUser()->isLoggedIn()) {
                try {
                    $user = $this->customer->getByEmail($values['email']);
                    $id = $user['user_id'];
                } catch (\Nette\InvalidArgumentException $ex) {
                    $user = $this->customer->insert($values, false);
                    $id = $user->user_id;
                }
            } else {
                $id = $this->getUser()->getId();
            }

            try {
                $values[\App\Model\Objednavka::CUSTOMER_ID] = $id;
                $order = $this->order->insert($values);

                $data = [];
                foreach ($this->shoppingCart->getShoppingList() as $id => $count) {
                    $data[] = [
                        \App\Model\ObjednavkaSortiment::ORDER_ID => $order->objednavka_id,
                        \App\Model\ObjednavkaSortiment::COUNT => $count,
                        \App\Model\ObjednavkaSortiment::SORTIMENT_ID => $id
                    ];
                }
                try {
                    $this->orderSortiment->insert($data);

                    $this->sortiment->removeProducts($this->shoppingCart->getShoppingList());

                    $this->shoppingCart->deleteShoppingList();

                    $this->redirect('Order:finished', ['id' => $order->string_id]);
                } catch (Exception $ex) {
                    $this->flashMessage('Objednávku se nepodařilo vytvořit');
                    if (!$this->getUser()->isLoggedIn()) {
                        $this->customer->delete($id);
                    }
                    $this->order->delete($order->objednavka_id);
                }
            } catch (Exception $ex) {
                if (!$this->getUser()->isLoggedIn()) {
                    $this->customer->delete($id);
                }
                $this->flashMessage('Nepodařilo se vytvořit objednávku');
            }
        } catch (Exception $ex) {
            $this->flashMessage('Při vytváření objednávky došlo k chybě');
        }
    }

}
