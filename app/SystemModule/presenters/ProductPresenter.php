<?php

namespace App\SystemModule\Presenters;

use Nette\Application\UI\Form as Form;

/**
 * Description of ProductPresenter
 *
 * @author user
 */
class ProductPresenter extends BasePresenter
{

    /** @var \App\Model\Sortiment @inject */
    public $sortiment;

    /** @var \App\Model\Kategorie @inject */
    public $category;

    /** @var \App\Model\Recenzia @inject */
    public $recenze;
    private $productId;
    private $categoryId;

    /** @var Array */
    private $data;

    public function actionDetail($id, $categoryId)
    {
        if ($id === null) {
            $this->flashMessage('V URL chybí ID.', 'alert alert-danger');

            if ($categoryId === null) {
                $this->redirect('Category:default');
            } else {
                $this->redirect('Category:category', ['id' => $categoryId]);
            }
        }

        $this->productId = $id;
        $this->categoryId = $categoryId;

        $this->data[0] = $this->category->getAll();
        $this->data[1] = $this->sortiment->getById($this->productId)->fetch();

        if ($this->data[1] === null) {
            $this->flashMessage('Produkt nebyl nalezen.', 'alert alert-danger');
            if ($categoryId === null) {
                $this->redirect('Category:default');
            } else {
                $this->redirect('Category:category', ['id' => $categoryId]);
            }
        }

        $this->data[2] = $this->recenze->getAll($this->productId)->fetchAll();
    }

    public function renderDetail()
    {
        $this->template->category = $this->data[0];
        $this->template->product = $this->data[1];
        $this->template->recenzeList = $this->data[2];
    }

//------HANDLE METHODS------------------------------------------------------
    public function handleBuy($id, $categoryId, $count)
    {
        if ($id === null || $categoryId === null) {
            $this->flashMessage('V URL chybí ID.', 'alert alert-danger');
            if ($id === null)
                $this->redirect('Homepage:');
            else
                $this->redirect('Category:category', ['id' => $categoryId]);
        }

        if ($count === null) {
            $count = 1;
        }

        $this->shoppingCart->updateShoppingList($id, $count);

        if (!$this->isAjax()) {
            $this->redirect('this');
        }
    }

    protected function createComponentRecenziaForm()
    {
        $form = new Form;

        $form->addTextArea('obsah')
                ->setRequired("Prosím zadejte obsah recenze");
        $form->addText('hodnotenie')
                ->setAttribute('type', 'number')
                ->setAttribute('placeholder', 'pocet hvezdicek (0-5)')
                ->addRule(Form::IS_IN, 'Zadané číslo není platené', [0, 1, 2, 3, 4, 5])
                ->setRequired('Prosím zadejte počet hvězček');
        $form->addSubmit('send');

        $form->onError[] = [$this, 'formError'];
        $form->onSuccess[] = [$this, 'recenziaForm'];

        return ($form);
    }

    public function recenziaForm(Form $form)
    {
        $values = $form->getValues();

        $values[\App\Model\Recenzia::SORTIMENT_ID] = $this->productId;
        $values[\App\Model\Recenzia::CUSTOMER_ID] = $this->getUser()->getId();

        $this->recenze->insert($values);

        $this->flashMessage('Recenze byla úspěšně přidána');
        
        $this->redirect('Product:detail', ['id' => $this->productId, 'categoryId' => $this->categoryId]);
    }

}
