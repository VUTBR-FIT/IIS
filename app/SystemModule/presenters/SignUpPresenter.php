<?php

namespace App\SystemModule\Presenters;

/**
 * Description of SignUpPresenter
 *
 * @author user
 */
class SignUpPresenter extends BasePresenter
{

    /** @var \Nette\Security\User @inject */
    public $user;

    /** @var \App\Forms\FormFactory @inject */
    public $formFactory;
    
    /** @var \App\Model\Zakaznik @inject */
    public $userModel;

    protected function createComponentSignUpForm()
    {
        $form = (new \App\Forms\SignUpFormFactory($this->formFactory, $this->user, $this->userModel))->create();
        $form->onError[] = array($this, 'formError');
        $form->onSuccess[] = array($this, 'signUpSuccess');
        return ($form);
    }

    public function signUpSuccess()
    {
        $this->flashMessage('Byl jste úspěšně registrován.');
        $this->redirect('Homepage:');
    }

}
