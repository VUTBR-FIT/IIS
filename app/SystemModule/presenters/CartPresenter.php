<?php

namespace App\SystemModule\Presenters;

/**
 * Description of Cart
 *
 * @author user
 */
class CartPresenter extends BasePresenter
{

    /** @var \App\Model\Sortiment @inject */
    public $sortiment;

    /** @var Array */
    private $products = array();

    public function actionDefault()
    {
        $cart = (array) $this->shoppingCart->getShoppingList();
        $products = $this->sortiment->getShoppingCart($cart)->fetchAll();

        foreach ($products as $product) {
            if (array_key_exists($product->sortiment_id, $cart)) {
                $temp['count'] = $cart[$product->sortiment_id];
                $temp['data'] = $product;
                $this->products[] = $temp;
            }
        }
    }

    public function renderDefault()
    {
        $this->template->products = $this->products;
    }

    public function handleRemove($id)
    {
        if ($id === null) {
            $this->flashMessage('Produkt nebyl nalezen');
        } else {
            $this->shoppingCart->removeShoppingList($id);
            $this->flashMessage('Produkt byl odebrán');
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        }
    }

}
