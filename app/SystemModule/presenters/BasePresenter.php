<?php

namespace App\SystemModule\Presenters;

use Nette;
use App\Forms;

/**
 * @autor Lukáš Černý (xcerny63)
 */
abstract class BasePresenter extends \App\Presenters\BasePresenter
{

    /** @var \Nette\Http\Session */
    private $session;

    /** @var App\Services\ShoppingCartService */
    protected $shoppingCart;

    /** Celková cená produktů v košíku */
    private $price;

    /** @var \App\Model\Sortiment @inject */
    public $sortiment;

    protected function startup()
    {
        parent::startup();

        $this->session = $this->getSession();

        if (!$this->session->isStarted()) {
            $this->session->start();
        }else if ($this->getUser() !== null && $this->getUser()->isLoggedIn() && !$this->getUser()->isInRole(\App\Model\User::ROLE_CUSTOMER)) {
            $this->getUser()->logout();
            $this->redirect('SignIn:default');
        }

        $this->shoppingCart = new \App\Services\ShoppingCartService($this->getSession());
    }

    public function beforeRender()
    {
        parent::beforeRender();


        $this->setLayout('layoutSystem');
        $this->getPrice();
        $this->template->shoppingPrice = $this->price;
        $this->template->cartProducts = $this->shoppingCart->getShoppingList();
        $this->template->time = new \Nette\Utils\DateTime;
    }

    private function getPrice()
    {
        $cart = (array) $this->shoppingCart->getShoppingList();

        if (count($cart)) {
            $products = $this->sortiment->getShoppingCart($cart, [\App\Model\Sortiment::PRICE, \App\Model\Sortiment::ID])->fetchAll();
            if (!$products) {
                $this->price = 0.0;
            } else {
                $col = \App\Model\Sortiment::PRICE;
                foreach ($products as $product) {
                    $this->price += $cart[$product->sortiment_id] * $product->$col;
                }
            }
        } else {
            $this->price = 0.0;
        }
    }

    public function handleLogout()
    {
        $this->getUser()->logout(true);

        if (!$this->isAjax())
            $this->redirect('this');
    }

}
