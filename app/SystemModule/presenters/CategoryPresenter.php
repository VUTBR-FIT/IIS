<?php

namespace App\SystemModule\Presenters;

/**
 * @autor Lukáš Černý (xcerny63)
 */
class CategoryPresenter extends BasePresenter
{

    /** @var \App\Model\Skicaky  */
    public $skicaky;

    /** @var \App\Model\Farbicky @inject */
    public $farbicky;

    /** @var \App\Model\Sortiment @inject */
    public $sortiment;

    /** @var \App\Model\Kategorie @inject */
    public $category;

    /** @var Integer */
    private $id;

    /** @var Array */
    private $data;

    //----- ACTION METHODS------------------------------------------------------
    public function actionDefault($id)
    {
        if ($id === null) {
            $this->flashMessage('V URL chybí ID.', 'alert alert-danger');
            $this->redirect('Category:default');
        }

        $this->id = $id;

        $this->setView('default');

        $this->data[0] = $this->category->getAll();
        $this->data[1] = $this->sortiment->getAll([\App\Model\Sortiment::CATEGORY_ID => $this->id], 50, 0, [\App\Model\Sortiment::NAME])->fetchAll();
    }

    public function actionCategory($id)
    {
        if ($id === null) {
            $this->flashMessage('V URL chybí ID.', 'alert alert-danger');
            $this->redirect('Category:default');
        }

        $this->id = $id;

        $this->setView('default');

        $this->data[0] = $this->category->getAll();
        $this->data[1] = $this->sortiment->getAll([\App\Model\Sortiment::CATEGORY_ID => $this->id], 50, 0, [\App\Model\Sortiment::NAME])->fetchAll();
    }

    //----- RENDER METHODS------------------------------------------------------
    public function renderDefault()
    {
        //var_dump($this->shoppingCart->getShoppingList());
        $this->template->category = $this->data[0];
        $this->template->products = $this->data[1];
    }

    //------HANDLE METHODS------------------------------------------------------
    public function handleBuy($id, $productId, $count)
    {
        if ($id === null || $productId === null) {
            $this->flashMessage('V URL chybí ID.', 'alert alert-danger');
            if ($id === null) $this->redirect ('Homepage:');
            else $this->redirect ('Category:category', ['id' => $id]);
        }

        if ($count === null) {
            $count = 1;
        }

        $this->shoppingCart->updateShoppingList($productId, $count);

        if (!$this->isAjax()) {
            $this->redirect('Category:category', ['id' => $id]);
        }
    }

    //------COMPONENT METHODS---------------------------------------------------
    //------FORM SUCCESS METHODS------------------------------------------------
    //----- PRIVATE METHODS-----------------------------------------------------
    private function createForm($values = null)
    {
        $form = new Form;

        $name = $form->addText('nazov')
                ->setRequired('Prosím zadejte jméno dodavatele.');
        if ($values !== null) {
            $name->setDefaultValue($values['nazov']);
        }

        $ic = $form->addText('cislo')
                ->setRequired('Prosím zadejte IC')
                ->addRule(Form::PATTERN, "Zadané IČ není správné", "[0-9]{6,15}");
        if ($values !== null) {
            $ic->setDefaultValue($values['cislo']);
        }

        $email = $form->addEmail('email')
                ->setRequired('Prosím zadejte kontaktní email');
        if ($values !== null) {
            $email->setDefaultValue($values['email']);
        }

        $transport = $form->addText('doprava')
                ->setRequired('Prosím zadejte druh dopravy');
        if ($values !== null) {
            $transport->setDefaultValue($values['doprava']);
        }

        $desc = $form->addText('poznamka')
                ->setAttribute('placeholder', 'nevyplněno');
        if ($values !== null) {
            $desc->setDefaultValue($values['poznamka']);
        }

        $form->addSubmit('send');

        $form->onError[] = array($this, 'formError');

        return ($form);
    }

}
