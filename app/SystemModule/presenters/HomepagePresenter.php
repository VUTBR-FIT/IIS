<?php

namespace App\SystemModule\Presenters;

/**
 * @autor Lukáš Černý (xcerny63)
 */
class HomepagePresenter extends BasePresenter
{

    /** @var \App\Model\Page @inject */
    public $page;

    /** @var \App\Model\Sortiment @inject */
    public $sortiment;

    /** @var String */
    private $name;

    /** @var Array */
    private $data;

    /** @var Array */
    private $products;

    public function actionDefault($id)
    {
        if ($id === null) {
            $this->redirect('Homepage:default', ['id' => 'domov']);
        } else {
            $this->name = $id;
        }

        $this->data = $this->page->getById($this->name)->fetch();

        if ($this->name == 'domov') {
            $this->products = $this->sortiment->getAll([], 4, 0, [\App\Model\Sortiment::DATE_ADD])->fetchAll();
        }

        if (!$this->data) {
            $this->redirect('this', ['id' => 'domov']);
        }
    }

    public function renderDefault()
    {
        $this->template->title = $this->data->title;
        $this->template->products = $this->products;
        $this->template->showProducts = ($this->name == 'domov') ? 1 : 0;
        $this->template->content = \Nette\Utils\Html::el()->setHtml($this->data->content);
    }

}
