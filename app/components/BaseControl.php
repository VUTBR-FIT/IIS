<?php

namespace App\Components;

use Nette\Application\UI\Control;

/**
 * Description of BaseControl
 *
 * @author user
 */
class BaseControl extends Control
{
    public function onFormError(\Nette\Application\UI\Form $form) 
    {
        foreach ($form->getErrors() as $error) {
            $this->flashMessage($error, 'error');
        }
    }
    
    public function templateRender($name)
    {
        //$this->template->setFile(__DIR__ . '/' . $name . '.latte');
        $this->template->render(__DIR__ . '/' . $name . '.latte');
    }
}
