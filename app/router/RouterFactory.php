<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;

class RouterFactory
{

    use Nette\StaticClass;

    /**
     * @return Nette\Application\IRouter
     */
    public static function createRouter()
    {
        $router = new RouteList;
        $admin = new RouteList('Admin');
        $admin[] = new Route('Admin/<presenter>/<action>[/<id>]', 'Homepage:default');
        $router[] = $admin;

        $system = new RouteList('System');
        $system[] = new Route('category[/<id>]', 'Category:default');
        $system[] = new Route('shopping-cart', 'Cart:default');
        $system[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
        $router[] = $system;

        return $router;
    }

}
