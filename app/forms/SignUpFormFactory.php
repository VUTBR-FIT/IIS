<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use Nette\Security\User;
use App\Model\User as UserModel;

class SignUpFormFactory
{

    use Nette\SmartObject;

    const PASSWORD_MIN_LENGTH = 7;

    /** @var FormFactory */
    private $factory;

    /** @var Nette\Security\User */
    private $userManager;

    /** @var \App\Model\Zakaznik */
    private $user;

    public function __construct(FormFactory $factory, Nette\Security\User $userManager, \App\Model\Zakaznik $user)
    {
        $this->user = $user;
        $this->factory = $factory;
        $this->userManager = $userManager;
    }

    /**
     * @return Form
     */
    public function create()
    {
        $form = $this->factory->create();

        $form->addText('email')
                ->setAttribute('placeholder', 'meno@domena.cz')
                ->setRequired('Zadajte Váš email.')
                ->addRule($form::EMAIL);

        $form->addPassword('password')
                ->setRequired('Vytvorte Váše heslo.')
                ->addRule($form::MIN_LENGTH, NULL, self::PASSWORD_MIN_LENGTH);

        $form->addText('cislo')
                ->setAttribute('type', 'phone')
                ->setAttribute('placeholder', '+420 816 42 42 42')
                ->setRequired('Zadajte Vaše telefonné číslo.');

        $form->addText('priezvisko')
                ->setRequired('Vyplňte Vaše meno.');

        $form->addSubmit('send');

        $form->onSuccess[] = function (Form $form) {
            $values = $form->getValues(true);
            try {
                $login = $values['email'];
                $pass = $values['password'];
                $values[UserModel::PRIVILEGES] = 2;

                $this->user->insert($values);
                $this->userManager->login($login, $pass);
            } catch (Nette\Database\UniqueConstraintViolationException $e) {
                try {
                    $user = $this->user->getByEmail($login);
                    if ($user['registrovan'] == 0) {
                        $values[UserModel::REGISTRED] = 1;
                        $this->user->update($user['user_id'], $values);
                        $this->userManager->login($login, $pass);
                    } else {
                        $form->addError('Uživatel s daným emailem již existuje');
                    }
                } catch (Exception $ex) {
                    
                }
            } catch (Model\DuplicateNameException $e) {
                $form->addError('Username is already taken.');
                return;
            }
        };
        return $form;
    }

}
