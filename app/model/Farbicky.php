<?php

namespace App\Model;

/**
 * @author Lukáš Černý (xcerny63)
 */
class Farbicky extends BaseModel
{

    const TABLE_NAME = 'farbicky';
    const ID = Sortiment::ID;
    const SIZE = "dlzka";
    const OPTIONS = "vlastnost";

    public function insert($data)
    {
        $sortiment = new Sortiment($this->getContext(), $this->container);
        $insert = $sortiment->insert($data);

        $col = Sortiment::ID;
        $data[self::ID] = $insert->$col;

        $data = array_intersect_key((array) $data, array_flip(self::getCols()));

        $this->getTable(self::TABLE_NAME)->insert($data);

        return ($data[self::ID]);
    }

    public function delete($id)
    {
        return ((new Sortiment($this->getContext(), $this->container))->delete($id));
    }

    public function update($id, $data)
    {
        $sortiment = new Sortiment($this->getContext(), $this->container);
        $sortiment->update($id, $data);

        $data = array_intersect_key((array) $data, array_flip(self::getCols()));

        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->update($data));
    }

    public function getById($id)
    {
        return ($this->query(self::getSQL() . " AND s." . Sortiment::ID . "=" . $id));
    }

    public function getAll($orderBy = Sortiment::NAME)
    {
        return ($this->query(self::getSQL() . " ORDER BY " . $orderBy));
    }

    public static function getCols()
    {
        return ([self::ID, self::SIZE, self::OPTIONS]);
    }

    public static function isSortiment(array $data)
    {
        $data = array_intersect_key((array) $data, array_flip(self::getCols()));
        return (count($data) > 1);
    }

    private static function getSQL()
    {
        $cols = self::getCols();
        array_shift($cols);
        return ("SELECT f." . implode(", f.", $cols) . ", s.* "
                . "FROM " . self::getTableName(self::TABLE_NAME) . " f "
                . "JOIN " . self::getTableName(Sortiment::TABLE_NAME) . " s "
                . "ON f." . self::ID . "=s." . Sortiment::ID . " "
                . "WHERE s." . Sortiment::DELETED . "=0");
    }

}
