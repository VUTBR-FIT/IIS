<?php

namespace App\Model;

/**
 * @author Lukáš Černý (xcerny63)
 */
class Objednavka extends BaseModel
{

    const ID = "objednavka_id";
    const CUSTOMER_ID = 'zakaznik_id';
    const EMPLOYYER = 'zamestnanec_id';
    const DATE = "datum";
    const STATE = "stav";
    const DESC = "poznamka";
    const TRANSPORT = "doprava_id";
    const PSC = 'psc',
            CITY = 'mesto',
            STREET = 'ulice',
            HOUSE_NUMBER = 'cislo_popisne',
            STRING_ID = 'string_id',
            DATE_CREATED = 'datum_vytvoreni';
    const TABLE_NAME = 'objednavka';

    public function insert($values)
    {
        $data = array_intersect_key((array) $values, array_flip(self::getCols()));

        $data[self::STRING_ID] = \Nette\Utils\Random::generate();
        $data[self::DATE_CREATED] = new \Nette\Utils\DateTime;

        return ($this->getTable(self::TABLE_NAME)->insert($data));
    }

    public function update($id, $values)
    {
        $data = array_intersect_key((array) $values, array_flip(self::getCols()));

        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->update($data));
    }

    public function delete($id)
    {
        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->delete());
    }

    public function getByStringId($id)
    {
        return ($this->getTable(self::TABLE_NAME)->where(self::STRING_ID, $id));
    }

    public function getAll($id, $orderBy = self::DATE_CREATED, $limit = 50)
    {
        $sql = "SELECT u.priezvisko, o.objednavka_id, o.doprava_id, o.stav, o.string_id, o.datum_vytvoreni, sum(os.mnozstvo * s.cena) as price FROM " . self::getTableName(self::TABLE_NAME) . " o "
                . " JOIN " . self::getTableName(ObjednavkaSortiment::TABLE_NAME) . " os ON o.objednavka_id=os.objednavka_id"
                . " JOIN " . self::getTableName(Sortiment::TABLE_NAME) . " s ON os.sortiment_id = s.sortiment_id"
                . " JOIN " . self::getTableName(Zakaznik::TABLE_NAME) . " u ON o.zakaznik_id=u.user_id"
                . (($id === null)? "": (" WHERE o.zakaznik_id=" . $id))
                . " GROUP BY u.priezvisko, o.doprava_id, o.stav, o.string_id, o.datum"
                . " ORDER BY " . $orderBy
                . " LIMIT " . $limit;
        return ($this->query($sql));
    }

    public static function getCols()
    {
        return ([self::ID, self::CUSTOMER_ID, self::DATE, self::DESC, self::EMPLOYYER, self::STATE, self::TRANSPORT, self::CITY, self::HOUSE_NUMBER, self::PSC, self::STREET]);
    }

}
