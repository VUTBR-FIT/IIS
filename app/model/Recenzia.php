<?php

namespace App\Model;

/**
 * @author Lukáš Černý (xcerny63)
 */
class Recenzia extends BaseModel
{

    const TABLE_NAME = "recenzia";
    const ID = "recenzia_id";
    const CUSTOMER_ID = Zakaznik::ID;
    const SORTIMENT_ID = Sortiment::ID;
    const DATE = "datum";
    const CONTENT = "obsah";
    const RATING = "hodnotenie";

    public function insert($data)
    {
        $data = array_intersect_key((array) $data, array_flip(self::getCols()));
        $data[self::DATE] = new \Nette\Utils\DateTime;
        return ($this->getTable(self::TABLE_NAME)->insert($data));
    }

    public function getAll($sortimentId, $orderBy = self::DATE)
    {
        $sql = "SELECT r.*, u.priezvisko FROM " . self::getTableName(self::TABLE_NAME) . " r"
                . " JOIN " . self::getTableName(Zakaznik::TABLE_NAME) . " u"
                . " ON r." . self::CUSTOMER_ID . " = u." . Zakaznik::ID
                . " WHERE r." . self::SORTIMENT_ID . "=" . $sortimentId
                . " ORDER BY " . $orderBy;
        return ($this->query($sql));
    }

    public static function getCols()
    {
        return ([self::ID, self::CUSTOMER_ID, self::SORTIMENT_ID, self::CONTENT, self::DATE, self::RATING]);
    }

}
