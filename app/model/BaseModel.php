<?php

namespace App\Model;

use Nette;

/**
 * @autor Lukáš Černý (xcerny63)
 */
class BaseModel extends \Nette\Object
{

    use Nette\SmartObject;

    /** @var string */
    protected static $prefix = '';

    /** @var Nette\DI\Container */
    protected $container;

    /** @var Nette\Database\Context */
    private $database;

    public function __construct(Nette\Database\Context $database, Nette\DI\Container $container)
    {
        $this->container = $container;
        $this->database = $database;
    }

    /**
     * @param string $name
     * @return Table\Selection
     */
    public function getTable($name)
    {
        return ($this->database->table($this->getTableName($name)));
    }

    public static function getTableName($name)
    {
        if (self::$prefix !== null) {
            $name = self::$prefix . $name;
        }
        return ($name);
    }

    public function query($sql)
    {
        return ($this->database->query($sql));
    }

    /**
     * @return Nette\Database\Context
     */
    public function getContext()
    {
        return ($this->database);
    }

}
