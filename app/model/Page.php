<?php

namespace App\Model;

/**
 * @author Lukáš Černý (xcerny63)
 */
class Page extends BaseModel
{

    const TABLE_NAME = 'page';
    const TITLE = "title";
    const ID = "url";
    const CONTENT = "content";

    public function insert($data)
    {
        $data = array_intersect_key((array) $data, array_flip(self::getCols()));

        return ($this->getTable(self::TABLE_NAME)->insert($data));
    }

    public function delete($id)
    {
        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->delete());
    }

    public function update($id, $data)
    {
        if (!$this->getTable(self::TABLE_NAME)->where(self::ID, $id)->fetch()) {
            throw new \Exception;
        }

        $data = array_intersect_key((array) $data, array_flip(self::getCols()));

        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->update($data));
    }

    public function getById($id)
    {
        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id));
    }

    public function getAll($orderBy = self::TITLE)
    {
        return ($this->getTable(self::TABLE_NAME)->order($orderBy));
    }

    public static function getCols()
    {
        return ([self::ID, self::TITLE, self::CONTENT]);
    }

}
