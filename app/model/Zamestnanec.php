<?php

namespace App\Model;

/**
 * @author Lukáš Černý (xcerny63)
 */
class Zamestnanec extends BaseModel
{

    const TABLE_NAME = "zamestnanec";
    const ID = "user_id";
    const NAME = "meno";
    const SURNAME = "priezvisko";
    const BIRTH_NUMBER = "rodne_cislo";
    const POSSITION = "pozicia";
    const PRIVILEGES = "opravnenie";

    public function insert($values)
    {
        $user = new User($this->getContext(), $this->container);
        $insert = $user->insert($values);

        $data = array_intersect_key((array) $values, array_flip(self::getCols()));

        $col = User::ID;
        $data[self::ID] = $insert->$col;

        return ($this->getTable(self::TABLE_NAME)->insert($data));
    }

    public function update($id, $data)
    {
        if (!$this->getTable(self::TABLE_NAME)->where(self::ID, $id)->fetch()) {
            throw new \Exception;
        }

        $user = new User($this->getContext(), $this->container);
        $user->update($id, $data);

        $data = array_intersect_key((array) $data, array_flip(self::getCols()));

        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->update($data));
    }

    public function getById($id)
    {
        return ($this->query(self::getSQL() . " AND z." . self::ID . " = " . $id));
    }

    public function getAll($orderBy = self::SURNAME)
    {
        return ($this->query(self::getSQL() . " ORDER BY " . $orderBy));
    }

    public static function getCols()
    {
        return ([self::ID, self::NAME, self::SURNAME, self::BIRTH_NUMBER, self::POSSITION, self::PRIVILEGES]);
    }

    private static function getSQL()
    {
        return ("SELECT z.*, u." . User::EMAIL . ", u." . User::PRIVILEGES
                . " FROM " . self::getTableName(self::TABLE_NAME) . " z"
                . " JOIN " . self::getTableName(User::TABLE_NAME) . " u"
                . " ON z." . self::ID . "=" . "u." . User::ID . ""
                . " WHERE u." . Sortiment::DELETED . "=0");
    }

}
