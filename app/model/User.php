<?php

namespace App\Model;

use Nette;
use Nette\Security\Passwords;

/**
 * @author Lukáš Černý (xcerny63)
 */
class User extends BaseModel implements \Nette\Security\IAuthenticator
{

    const TABLE_NAME = "user";
    const
            ID = "user_id",
            EMAIL = "email",
            PASS = "password",
            PRIVILEGES = 'role',
            REGISTRED = 'registrovan',
            DELETED = "smazano";
    const
            ROLE_MAIN_ADMIN = 'main_admin',
            ROLE_EMPLOYEE = 'employee',
            ROLE_CUSTOMER = 'customer',
            ROLE_GUEST = 'guest';

    /**
     * @param array $credentials
     * @return type
     * @throws \Nette\InvalidArgumentException
     */
    public function authenticate(array $credentials)
    {
        list($email, $pass) = $credentials;

        $row = $this->getTable(self::TABLE_NAME)->where(self::EMAIL, $email)->where(self::DELETED, 0)->where(self::REGISTRED, 1)->fetch();

        if (!$row) {
            throw new \Nette\InvalidArgumentException('Neznámé uživatelské jméno');
        } elseif (!\Nette\Security\Passwords::verify($pass, $row[self::PASS])) {
            throw new \Nette\InvalidArgumentException('Špatné heslo.');
        } elseif (\Nette\Security\Passwords::needsRehash($row[self::PASS])) {
            $row->update(array(
                self::PASS => \Nette\Security\Passwords::hash($pass)
            ));
        }

        $user = null;
        $array = $row->toArray();
        if ($array[self::PRIVILEGES] > 1) {
            $user = $this->getTable(Zakaznik::TABLE_NAME)->where(Zakaznik::ID, $array[self::ID])->fetch();
        } else {
            $user = $this->getTable(Zamestnanec::TABLE_NAME)->where(Zamestnanec::ID, $array[self::ID])->fetch();
        }

        $userArr = $user->toArray();

        unset($array[self::PASS]);
        $userArr[self::PRIVILEGES] = self::roleToString($array[self::PRIVILEGES]);

        return (new \Nette\Security\Identity($userArr[self::ID], ($userArr[self::PRIVILEGES]), $userArr));
    }

    public function insert($values)
    {
        $keys = self::getCols();

        $data = array_intersect_key((array) $values, array_flip($keys));
        if (array_key_exists(self::PASS, $data)) {
            $data[self::PASS] = \Nette\Security\Passwords::hash($data[self::PASS]);
        }

        if (!array_key_exists(self::PRIVILEGES, $data)) {
            $data[self::PRIVILEGES] = -1;
        }
        $data[self::PRIVILEGES] = self::checkRoleInteger($data[self::PRIVILEGES]);

        return ($this->getTable(self::TABLE_NAME)->insert($data));
    }

    public function update($id, $data)
    {
        if (!$this->getTable(self::TABLE_NAME)->where(self::ID, $id)->where(self::DELETED, 0)->fetch()) {
            throw new \Exception;
        }
        $keys = self::getCols();

        $data = array_intersect_key((array) $data, array_flip($keys));
        if (array_key_exists(self::PRIVILEGES, $data)) {
            $data[self::PRIVILEGES] = self::checkRoleInteger($data[self::PRIVILEGES]);
        }

        if (array_key_exists(self::PASS, $data))
            $data[self::PASS] = \Nette\Security\Passwords::hash($data[self::PASS]);

        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->update($data));
    }

    public function delete($id)
    {
        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->update([self::DELETED => 1]));
    }

    private static function getSQLCustomesrs($select = 'z.*')
    {
        return (self::getSQLUser(Zakaznik::TABLE_NAME, $select));
    }

    private static function getSQLEmployers($select = 'z.*')
    {
        return (self::getSQLUser(Zamestnanec::TABLE_NAME, $select));
    }

    private static function getSQLUser($table, $select = 'z.*')
    {
        return ("SELECT u.email, u.password, u.role, {$select} FROM user u JOIN {$table} z ON u.user_id = z.user_id");
    }

    public function getUserByEmail($email)
    {
        $user = $this->getTable(self::TABLE_NAME)->where(self::EMAIL, $email)->where(self::DELETED, 0)->fetch();
        if ($user === false) {
            throw new \Nette\InvalidArgumentException('Neznámé uživatelské jméno');
        } elseif ($user->role == 2) {
            $tempCol = self::ID;
            $temp = $this->getTable(Zakaznik::TABLE_NAME)->where(Zakaznik::ID, $user->$tempCol)->fetch();
            $tempArr = $temp->toArray();
            $tempArr[self::EMAIL] = $email;
            $tempArr[self::PRIVILEGES] = self::ROLE_GUEST;
            $tempArr[self::REGISTRED] = $user[self::REGISTRED];
            return ($tempArr);
        } else {
            $tempCol = self::ID;
            $temp = $this->getTable(Zamestnanec::TABLE_NAME)->where(Zamestnanec::ID, $user->$tempCol)->fetch();
            $tempArr = $temp->toArray();
            $tempArr[self::EMAIL] = $email;
            $tempArr[self::PRIVILEGES] = self::ROLE_GUEST;
            return ($tempArr);
        }
    }

    private function checkRoleInteger($role)
    {
        return (($role > 2 || $role < 0) ? 3 : $role);
    }

    private static function roleToString($intRole)
    {
        switch ($intRole) {
            case 0:
                return (self::ROLE_MAIN_ADMIN);
            case 1:
                return (self::ROLE_EMPLOYEE);
            case 2:
                return (self::ROLE_CUSTOMER);
            default:
                return (self::ROLE_GUEST);
        }
    }

    public static function getCols()
    {
        return ([self::ID, self::EMAIL, self::PASS, self::PRIVILEGES, self::DELETED, self::REGISTRED]);
    }

}
