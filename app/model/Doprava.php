<?php

namespace App\Model;

/**
 * Description of Doprava
 *
 * @author user
 */
class Doprava extends BaseModel
{

    const ID = "doprava_id";
    const NAME = "nazov";
    const PRICE = "cena";
    const TABLE_NAME = "doprava";

    public function getAll($orderBy = self::NAME)
    {
        return ($this->getTable(self::TABLE_NAME)->order($orderBy));
    }

    public static function getCols()
    {
        return ([self::ID, self::NAME, self::PRICE]);
    }

}
