<?php

namespace App\Model;

/**
 * @author Lukáš Černý (xcerny63)
 */
class Dodavatel extends BaseModel
{
    const TABLE_NAME = 'dodavatel';
    const ID = "dodavatel_id";
    const NAME = "nazov";
    const NUMBER = "cislo";
    const EMAIL = "email";
    const TRANSPORT = "doprava";
    const DESC = "poznamka";
    const DELETED = 'smazano';

    
    
    public function insert($data)
    {
        $data = array_intersect_key((array) $data, array_flip(self::getCols()));

        return ($this->getTable(self::TABLE_NAME)->insert($data));
    }

    public function delete($id)
    {
        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->where([self::DELETED => 0])->update([self::DELETED => 1]));
    }

    public function update($id, $data)
    {
        if (!$this->getTable(self::TABLE_NAME)->where(self::ID, $id)->where([self::DELETED => 0])->fetch()) {
            throw new \Exception;
        }
        
        $data = array_intersect_key((array) $data, array_flip(self::getCols()));
        
        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->update($data));
    }

    public function getById($id)
    {
        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id));
    }
    
    public function getAll($orderBy = self::NAME)
    {
        return ($this->getTable(self::TABLE_NAME)->where([self::DELETED => 0])->order($orderBy));
    }

    public static function getCols()
    {
        return ([self::ID, self::NAME, self::NUMBER, self::EMAIL, self::TRANSPORT, self::DESC, self::DELETED]);
    }

}
