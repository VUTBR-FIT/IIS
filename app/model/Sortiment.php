<?php

namespace App\Model;

/**
 * @author Lukáš Černý (xcerny63)
 */
class Sortiment extends BaseModel
{

    const TABLE_NAME = "sortiment";
    const ID = "sortiment_id";
    const DELIVERY_ID = Dodavatel::ID;
    const CATEGORY_ID = "kategorie_id";
    const COUNT = "pocet";
    const TYPE = "typ";
    const NAME = "nazov";
    const STATE = "dostupnost";
    const COLOR = "farba";
    const PRICE = "cena";
    const DELETED = 'smazano';
    const DATE_ADD = 'pridano';
    const ICON = "icon";

    public function insert($data)
    {
        $data = array_intersect_key((array) $data, array_flip(self::getCols()));
        $data[self::DATE_ADD] = new \Nette\Utils\DateTime();

        if (array_key_exists(self::ICON, $data)) {
            $file = $data[self::ICON];
            if ($file->isImage() && $file->isOk()) {
                $fileExt = strtolower(mb_substr($file->getSanitizedName(), strrpos($file->getSanitizedName(), '.')));
                $data[self::ICON] = \Nette\Utils\Random::generate() . $fileExt;
                $file->move($this->container->getParameters()['wwwDir'] . "/upload/" . $data[self::ICON]);
            } else {
                unset($data[self::ICON]);
            }
        }
        return ($this->getTable(self::TABLE_NAME)->insert($data));
    }

    public function delete($id)
    {
        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->update([self::DELETED => 1]));
    }

    public function update($id, $data)
    {
        if (!$this->getTable(self::TABLE_NAME)->where(self::ID, $id)->where(self::DELETED, 0)->fetch()) {
            throw new \Exception;
        }

        $data = array_intersect_key((array) $data, array_flip(self::getCols()));

        if (array_key_exists(self::ICON, $data)) {
            $file = $data[self::ICON];
            if ($file->isImage() && $file->isOk()) {
                $fileExt = strtolower(mb_substr($file->getSanitizedName(), strrpos($file->getSanitizedName(), '.')));
                $data[self::ICON] = \Nette\Utils\Random::generate() . $fileExt;
                $file->move($this->container->getParameters()['wwwDir'] . "/upload/" . $data[self::ICON]);
            } else {
                unset($data[self::ICON]);
            }
        }

        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->where(self::DELETED, 0)->update($data));
    }

    public function getById($id)
    {
        $temp = new Farbicky($this->getContext(), $this->container);
        $row = $temp->getById($id)->fetch();
        if ($row === false) {
            $temp = new Skicaky($this->getContext(), $this->container);
        }
        return ($temp->getById($id));
    }

    public function getAll($where = array(), $limit = 50, $offset = 0, $orderBy = array())
    {
        /** @var \Nette\Database\Table\Selection */
        $query = $this->getTable(self::TABLE_NAME)->where(self::DELETED, 0);
        if (is_array($where) && count($where)) {
            foreach ($where as $key => $value) {
                $query->where($key, $value);
            }
        }
        if (count($orderBy) > 0) {
            $query->order(implode(', ', $orderBy));
        }


        $query->limit($limit, $offset);
        return ($query);
    }

    public function getShoppingCart($shoppingCart, $cols = array())
    {
        /** @var \Nette\Database\Table\Selection */
        $query = $this->getTable(self::TABLE_NAME)->where(self::DELETED, 0);

        if (count($cols)) {
            $query->select(implode(', ', $cols));
        } else {
            $query->select('*');
        }

        if (is_array($shoppingCart) && count($shoppingCart)) {
            $temp = "";
            foreach ($shoppingCart as $key => $value) {
                $temp .= $key . ", ";
            }
            $query->where(self::ID . " IN (" . substr($temp, 0, strlen($temp) - 2) . ")");
        }
        $query->order(self::NAME);

        return ($query);
    }

    public function removeProducts($values)
    {
        foreach ($values as $key => $count) {
            $sql = "UPDATE " . self::getTableName(self::TABLE_NAME)
                    . " SET " . self::COUNT . " = " . self::COUNT . "-" . $count . " WHERE " . self::ID . " = " . $key;
            $this->query($sql);
        }
    }

    public static function getCols()
    {
        return ([self::ID, self::DELIVERY_ID, self::COUNT, self::TYPE, self::NAME, self::STATE, self::COLOR, self::PRICE, self::CATEGORY_ID, self::DELETED, self::DATE_ADD, self::ICON]);
    }

}
