<?php

namespace App\Model;

/**
 * @author Lukáš Černý (xcerny63)
 */
class DodavatelObjednavky extends BaseModel
{

    const TABLE_NAME = 'dodavatel_objednavka';
    const ID = "dodavatel_objednavka_id";
    const DODAVATEL = "dodavatel_id";
    const SORTIMENT = "sortiment_id";
    const COUNT = "kusy";
    const DATE_DELIVERY = "datum_doruceni";
    const DATE_CREATED = "datum_vytvoreni";
    const STATE = "stav";
    const SUM = "celkem";

    public function insert($data)
    {
        $data = array_intersect_key((array) $data, array_flip(self::getCols()));

        $data[self::STATE] = 0;
        $data[self::DATE_CREATED] = new \Nette\Utils\DateTime();

        return ($this->getTable(self::TABLE_NAME)->insert($data));
    }

    public function delete($id)
    {
        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->delete());
    }

    public function update($id, $data)
    {
        if (!$this->getTable(self::TABLE_NAME)->where(self::ID, $id)->fetch()) {
            throw new \Exception;
        }

        $data = array_intersect_key((array) $data, array_flip(self::getCols()));

        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->update($data));
    }

    public function getById($id)
    {
        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id));
    }

    public function getAll($orderBy = self::DATE_CREATED)
    {
        $sql = "SELECT d.*, s.nazov, x.nazov as dodavatel"
                . " FROM " . self::getTableName(self::TABLE_NAME) . " d"
                . " JOIN " . self::getTableName(Sortiment::TABLE_NAME) . " s"
                . " ON d." . self::SORTIMENT . "=s." . Sortiment::ID 
                . " JOIN " . self::getTableName(Dodavatel::TABLE_NAME) . " x"
                . " ON d." .self::DODAVATEL . "=x." . Dodavatel::ID
                . " ORDER BY d.stav, d." . $orderBy;
        return ($this->query($sql));
    }

    public static function getCols()
    {
        return ([self::ID, self::DODAVATEL, self::SORTIMENT, self::COUNT, self::DATE_DELIVERY, self::DATE_CREATED, self::SUM, self::STATE]);
    }

}
