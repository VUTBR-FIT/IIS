<?php

namespace App\Model;

/**
 * @author Lukáš Černý (xcerny63)
 */
class ObjednavkaSortiment extends BaseModel
{

    const TABLE_NAME = 'objednavka_sortiment';
    const ID = "objednavka_sortiment_id";
    const SORTIMENT_ID = Sortiment::ID;
    const ORDER_ID = Objednavka::ID;
    const COUNT = "mnozstvo";

    public function insert($values)
    {
        return ($this->getTable(self::TABLE_NAME)->insert($values));
    }

    public function getByOrderId($id)
    {
        $sql = "SELECT o.mnozstvo, s.* FROM " . self::getTableName(self::TABLE_NAME) . " o JOIN " . self::getTableName(Sortiment::TABLE_NAME) . " s ON o." . self::SORTIMENT_ID . "=s." . Sortiment::ID
                . " WHERE o." . self::ORDER_ID . "=" . $id . " "
                . " ORDER BY s." . Sortiment::NAME;
        
        return ($this->query($sql));
    }

}
