<?php

namespace App\Model;

/**
 * @author Lukáš Černý (xcerny63)
 */
class Kategorie extends BaseModel
{

    const ID = "category_id";
    const NAME = "name";
    const PARENT_ID = "parent_id";
    const TABLE_NAME = "kategorie";

    private $temp;

    public function insert($data)
    {
        $data = array_intersect_key((array) $data, array_flip(self::getCols()));

        return ($this->getTable(self::TABLE_NAME)->insert($data));
    }

    public function delete($id)
    {
        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->delete());
    }

    public function update($id, $data)
    {
        if (!$this->getTable(self::TABLE_NAME)->where(self::ID, $id)->fetch()) {
            throw new \Exception;
        }
        $keys = self::getCols();

        $data = array_intersect_key((array) $data, array_flip($keys));

        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->update($data));
    }

    public function getById($id)
    {
        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id));
    }
    
    public function getAll($mainCategory = false)
    {
        $this->temp = $this->getTable(self::TABLE_NAME)->order(self::PARENT_ID)->fetchAll();

        $result = [];
        if ($mainCategory) {
            $result[0] = [
                'data' => (object) [
                    self::NAME => "Hlavní menu",
                    self::ID => 0
                ],
                'subMenu' => array()
            ];
        }

        foreach ($this->temp as $key => $cat) {
            if ($cat[self::PARENT_ID] == null) {
                unset($this->temp[$key]);
                $result[$cat[self::ID]] = [
                    'data' => $cat,
                    'subMenu' => $this->subMenu($cat[self::ID])
                ];
            }
        }
        return ($result);
    }

    public function upgradeLevel($id)
    {
        $row = $this->getTable(self::TABLE_NAME)->where(self::ID, $id)->fetch();
        if (!$row)
            throw new \Exception;

        $temp = $this->getTable(self::TABLE_NAME)->where(self::ID, $row[self::PARENT_ID])->fetch();
        if (!$temp)
            throw new \Exception;

        return ($this->update($id, [self::PARENT_ID => $temp[self::PARENT_ID]]));
    }

    private function subMenu($parrentId)
    {
        $result = [];
        foreach ($this->temp as $key => $cat) {
            if ($cat[self::PARENT_ID] != null && $cat[self::PARENT_ID] == $parrentId) {
                unset($this->temp[$key]);
                $result[$cat[self::ID]] = [
                    'data' => $cat,
                    'subMenu' => $this->subMenu($cat[self::ID])
                ];
            }
        }
        return ($result);
    }

    private static function getCols()
    {
        return ([self::ID, self::NAME, self::PARENT_ID]);
    }

}
