<?php

namespace App\Model;

/**
 * @author Lukáš Černý (xcerny63)
 */
class Zakaznik extends BaseModel
{

    const ID = "user_id";
    const SURNAME = "priezvisko";
    const NUMBER = "cislo";
    const REGISTRATION = "registracia";
    const TABLE_NAME = "zakaznik";

    public function insert($values, $registration = true)
    {
        $user = new User($this->getContext(), $this->container);
        if ($registration === false) {
            $values[User::REGISTRED] = 0;
        }
        $values[User::PRIVILEGES] = 2;
        $insert = $user->insert($values);

        $data = array_intersect_key((array) $values, array_flip(self::getCols()));

        $col = User::ID;
        $data[self::ID] = $insert->$col;
        if ($registration === true) {
            $data[self::REGISTRATION] = new \Nette\Utils\DateTime;
        }

        return ($this->getTable(self::TABLE_NAME)->insert($data));
    }

    public function update($id, $data)
    {
        if (!$this->getTable(self::TABLE_NAME)->where(self::ID, $id)->fetch()) {
            throw new \Exception;
        }

        $user = new User($this->getContext(), $this->container);
        $user->update($id, $data);

        $data = array_intersect_key((array) $data, array_flip(self::getCols()));

        return ($this->getTable(self::TABLE_NAME)->where(self::ID, $id)->update($data));
    }

    public function delete($id)
    {
        $user = new App\Model\User($this->database, $this->container);
        return ($user->delete($id));
    }

    public function getByEmail($email)
    {
        $user = new User($this->getContext(), $this->container);

        return ($user->getUserByEmail($email));
    }

    public function getById($id)
    {
        $sql = "SELECT u.*, z.cislo, z.priezvisko, z.registracia FROM " . self::getTableName(self::TABLE_NAME) . " z"
                . " JOIN " . self::getTableName(User::TABLE_NAME) . " u ON z." . self::ID . "=u." . User::ID
                . " WHERE u." . User::ID . "=" . $id;
        return ($this->query($sql));
    }

    public static function getCols()
    {
        return ([self::ID, self::SURNAME, self::NUMBER, self::REGISTRATION]);
    }

}
