<?php

namespace App\AdminModule\Presenters;

use \Nette\Application\UI\Form as Form;

/**
 * Description of Supplier
 *
 * @author Lukáš Černý (xcerny63)
 */
class SupplierPresenter extends BasePresenter
{

    /** @var \App\Model\Dodavatel @inject */
    public $transport;

    /** @var integer */
    private $id = null;

    /** @var array */
    private $data = array();

    //----- ACTION METHODS------------------------------------------------------
    public function actionDefault()
    {
        $this->redirect('list');
    }

    public function actionList()
    {
        $this->data = $this->transport->getAll();
    }

    public function actionUpdate($id)
    {
        if (!$id) {
            $this->flashMessage('V URL chybí ID.', 'alert alert-danger');
            $this->redirect('Supplier:list');
        }
        $this->id = $id;

        $this->data = $this->transport->getById($this->id)->fetch();

        if (!$this->data) {
            $this->flashMessage('Dodavatel nebyl nalezen.', 'alert alert-danger');
            $this->redirect('Supplier:list');
        }
    }

    //----- RENDER METHODS------------------------------------------------------
    public function renderList()
    {
        $this->template->list = $this->data;
    }

    //------HANDLE METHODS------------------------------------------------------
    public function handleDelete($id)
    {
        if (!$id) {
            $this->flashMessage('Dodavatele se nepodařilo smazat.', 'alert alert-danger');
        } else {
            try {
                $this->transport->delete($id);
                $this->flashMessage('Dodavatel byl úspěšně smazán.', 'alert alert-success');
            } catch (\Exception $ex) {
                $this->flashMessage('Dodavatele se nepodařilo smazat.', 'alert alert-danger');
            }
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        }
    }

    //------COMPONENT METHODS---------------------------------------------------
    protected function createComponentAddSupplier()
    {
        $form = $this->createForm();

        $form->onSuccess[] = array($this, 'formAddSupplier');

        return ($form);
    }

    protected function createComponentUpdateSupplier()
    {
        $form = $this->createForm($this->data);

        $form->onSuccess[] = array($this, 'formUpdateSupplier');

        return ($form);
    }

    //------FORM SUCCESS METHODS------------------------------------------------
    public function formAddSupplier(Form $form)
    {
        $values = $form->getValues();

        try {
            $this->transport->insert($values);
            $this->flashMessage("Dodavatel byl úspěšně přidán.", 'alert alert-success');
            $this->redirect("Supplier:list");
        } catch (\Nette\Database\UniqueConstraintViolationException $e) {
            $this->flashMessage("Dodavatele se nepodařilo přidat.", 'alert alert-danger');
        }
    }

    public function formUpdateSupplier(Form $form)
    {
        $values = $form->getValues();

        try {
            $this->transport->update($this->id, $values);
            $this->flashMessage("Dodavatel byl úspěšně upraven.", 'alert alert-success');
            $this->redirect("Supplier:list");
        } catch (\Nette\Database\UniqueConstraintViolationException $e) {
            $this->flashMessage("Dodavatele se nepodařilo upravit.", 'alert alert-danger');
        }
    }

    //----- PRIVATE METHODS-----------------------------------------------------
    private function createForm($values = null)
    {
        $form = new Form;

        $name = $form->addText('nazov')
                ->setRequired('Prosím zadejte jméno dodavatele.');
        if ($values !== null) {
            $name->setDefaultValue($values['nazov']);
        }

        $ic = $form->addText('cislo')
                ->setRequired('Prosím zadejte IC')
                ->addRule(Form::PATTERN, "Zadané IČ není správné", "[0-9]{6,15}");
        if ($values !== null) {
            $ic->setDefaultValue($values['cislo']);
        }

        $email = $form->addEmail('email')
                ->setRequired('Prosím zadejte kontaktní email');
        if ($values !== null) {
            $email->setDefaultValue($values['email']);
        }

        $transport = $form->addText('doprava')
                ->setRequired('Prosím zadejte druh dopravy');
        if ($values !== null) {
            $transport->setDefaultValue($values['doprava']);
        }

        $desc = $form->addText('poznamka')
                ->setAttribute('placeholder', 'nevyplněno');
        if ($values !== null) {
            $desc->setDefaultValue($values['poznamka']);
        }

        $form->addSubmit('send');

        $form->onError[] = array($this, 'formError');

        return ($form);
    }

}
