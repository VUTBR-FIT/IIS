<?php

namespace App\AdminModule\Presenters;

use \Nette\Application\UI\Form as Form;

/**
 * Description of EmployeePresenter
 *
 * @author Lukáš Černý (xcerny63)
 */
class EmployeePresenter extends BasePresenter
{

    /** @var \App\Model\Zamestnanec @inject */
    public $employee;

    /** @var \App\Model\User @inject */
    public $user;

    /** @var integer */
    private $id = null;

    /** @var array */
    private $data = array();

    protected function startup()
    {
        parent::startup();

        if (!$this->getUser()->isInRole(\App\Model\User::ROLE_MAIN_ADMIN)) {
            $this->flashMessage("Do této části nemáte přistup.", 'alert alert-danger');
            $this->redirect('Homepage:default');
        }
    }

    //----- ACTION METHODS------------------------------------------------------
    public function actionDefault()
    {
        $this->redirect('list');
    }

    public function actionList()
    {
        $this->data = $this->employee->getAll();
    }

    public function actionUpdate($id)
    {
        if (!$id) {
            $this->flashMessage('V URL chybí ID.', 'alert alert-danger');
            $this->redirect('Employee:list');
        }
        $this->id = $id;

        $this->data = $this->employee->getById($this->id)->fetch();

        if (!$this->data) {
            $this->flashMessage('Uživatel nebyl nalezen.', 'alert alert-danger');
            $this->redirect('Employee:list');
        }
    }

    public function renderList()
    {
        $this->template->list = $this->data;
    }

    //------HANDLE METHODS------------------------------------------------------
    public function handleChangeRole($id, $role)
    {
        if ($id === null || $role === null) {
            $this->flashMessage('Uživateli se nepodařilo změnit práva.', 'alert alert-danger');
        } else {
            try {
                $this->user->update($id, [\App\Model\User::PRIVILEGES => $role]);
                $this->flashMessage('Uživateli bylo změněna role.', 'alert alert-success');
            } catch (\Exception $ex) {
                $this->flashMessage('Uživatel nebyl v databázi nalezen.', 'alert alert-danger');
            }
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        }
    }

    public function handleDelete($id)
    {
        if (!$id) {
            $this->flashMessage('Uživateli se nepodařilo smazat.', 'alert alert-danger');
        } else {
            try {
                $this->user->delete($id);
                $this->flashMessage('Uživateli byl úspěšně odstraněn.', 'alert alert-success');
            } catch (\Exception $ex) {
                $this->flashMessage('Uživatel nebyl v datanázi nalezen.', 'alert alert-danger');
            }
        }

        if (!$this->isAjax()) {
            $this->redirect('this');
        }
    }

//------COMPONENT METHODS---------------------------------------------------
    protected function createComponentAddEmployee()
    {
        $form = $this->createForm();

        $form->onSuccess[] = array($this, 'formAddEmployee');

        return ($form);
    }

    protected function createComponentUpdateEmployee()
    {
        $form = $this->createForm($this->data);

        $form->onSuccess[] = array($this, 'formUpdateEmployee');

        return ($form);
    }

    //------FORM SUCCESS METHODS------------------------------------------------
    public function formAddEmployee(Form $form)
    {
        $values = $form->getValues();

        if ($values->password != $values->password_again) {
            $this->flashMessage("Hesla se neshodují", 'alert alert-warning');
        } else {
            try {
                $this->employee->insert($values);
                $this->flashMessage("Zaměstnanec by úspěšně přidán", 'alert alert-success');
                $this->redirect("Employee:list");
            } catch (\Nette\Database\UniqueConstraintViolationException $e) {
                $this->flashMessage("V systému je tento mail již využíván", 'alert alert-danger');
            }
        }
    }

    public function formUpdateEmployee(Form $form)
    {
        $values = $form->getValues();

        if ($values->password != $values->password_again) {
            $this->flashMessage("Hesla se neshodují", 'alert alert-warning');
        } else {
            try {
                if (strlen($values['password']) == 0) {
                    unset($values['password']);
                }
                $this->employee->update($this->id, $values);
                $this->flashMessage("Zaměstnanec by úspěšně upraven", 'alert alert-success');
                $this->redirect("Employee:list");
            } catch (\Nette\Database\UniqueConstraintViolationException $e) {
                $this->flashMessage("V systému je tento mail již využíván", 'alert alert-danger');
            }
        }
    }

    //----- PRIVATE METHODS-----------------------------------------------------
    private function createForm($values = null)
    {
        $form = new Form;

        $email = $form->addEmail('email')
                ->setAttribute('placeholder', 'email@domena.com')
                ->setRequired('Zadejte Váš email.');
        if ($values !== null) {
            $email->setDefaultValue($values['email']);
        }

        $pass = $form->addText('password')
                ->setAttribute('type', 'password');
        if ($values !== null) {
            $pass->setAttribute('placeholder', 'nezměněno');
        } else {
            $pass->setRequired("Prosím zadejte vaše heslo");
        }

        $form->addText('password_again')
                ->setRequired(FALSE)
                ->setAttribute('type', 'password')
                ->addRule(Form::EQUAL, "Hesla se neshodují", $form['password']);

        $name = $form->addText('meno')
                ->setRequired("Prosím zadejte vaše jméno");
        if ($values !== null) {
            $name->setDefaultValue($values['meno']);
        }

        $surname = $form->addText('priezvisko')
                ->setRequired("Prosím zadejte vaše příjmení");
        if ($values !== null) {
            $surname->setDefaultValue($values['priezvisko']);
        }

        $birthNumber = $form->addText('rodne_cislo')
                ->setAttribute('placeholder', 'YYMMDDXXXX')
                ->setRequired("Prosím zadejte vaše rodné číslo")
                ->addRule(Form::PATTERN, "Zadaná hodnota neodpovídá rodnému číslu", '[0-9]{10}');
        if ($values !== null) {
            $birthNumber->setDefaultValue($values['rodne_cislo']);
        }

        $place = $form->addText('pozicia')
                ->setRequired("Prosím zadejte pracovní pozici zaměstnance");
        if ($values !== null) {
            $place->setDefaultValue($values['pozicia']);
        }

        $role = $form->addSelect('role', NULL, array(
                    0 => 'Hlavní Admin',
                    1 => 'Zaměstnanec',
                ))
                ->setRequired("Prosím zadejste roli zaměstnance");
        if ($values !== null) {
            $role->setDefaultValue($values['role']);
        }

        $form->addSubmit('send');

        $form->onError[] = array($this, 'formError');

        return ($form);
    }

}
