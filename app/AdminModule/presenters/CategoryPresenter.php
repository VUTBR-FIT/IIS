<?php

namespace App\AdminModule\Presenters;

use \Nette\Application\UI\Form as Form;

/**
 * Description of Category
 *
 * @author Lukáš Černý (xcerny63)
 */
class CategoryPresenter extends BasePresenter
{

    /** @var \App\Model\Kategorie @inject */
    public $category;

    /** @var \App\Model\Sortiment @inject */
    public $sortiment;

    /** @var integer */
    private $id = null;

    /** @var array */
    private $data = array();

    /** @var array */
    private $categorie = array();

    //----- ACTION METHODS------------------------------------------------------
    public function actionDefault()
    {
        $this->redirect('list');
    }

    public function actionList()
    {
        $this->categorie = $this->category->getAll();
    }

    public function actionAdd()
    {
        $data = $this->category->getAll(true);
        foreach ($data as $value) {
            if ($this->id === null || $this->id != $value['data']->category_id) {
                $this->categorie[$value['data']->category_id] = $value['data']->name;
                if (count($value['subMenu'])) {
                    $this->createSelect($value['data']->name . "->", $value['subMenu']);
                }
            }
        }
    }

    public function actionUpdate($id)
    {
        if (!$id) {
            $this->flashMessage('V URL chybí ID.', 'alert alert-danger');
            $this->redirect('Category:list');
        }
        $this->id = $id;

        $this->data = $this->category->getById($this->id)->fetch();
        if (!$this->data) {
            $this->flashMessage('Kategorie nebyla nalezena.', 'alert alert-danger');
            $this->redirect('Category:list');
        }

        $data = $this->category->getAll(true);
        foreach ($data as $value) {
            if ($this->id === null || $this->id != $value['data']->category_id) {
                $this->categorie[$value['data']->category_id] = $value['data']->name;
                if (count($value['subMenu'])) {
                    $this->createSelect($value['data']->name . "->", $value['subMenu']);
                }
            }
        }
    }

    public function actionDetail($id)
    {
        if (!$id) {
            $this->flashMessage('V URL chybí ID.', 'alert alert-danger');
            $this->redirect('Category:list');
        }
        $this->id = $id;

        $this->categorie = $this->category->getById($this->id)->fetch();

        if (!$this->categorie) {
            $this->flashMessage('Kategorie nebyla nalezena.', 'alert alert-danger');
            $this->redirect('Category:list');
        }

        $this->data = $this->sortiment->getAll([\App\Model\Sortiment::CATEGORY_ID => $this->id]);
    }

    //----- RENDER METHODS------------------------------------------------------
    public function renderList()
    {
        $this->template->list = $this->categorie;
    }

    public function renderDetail()
    {
        $this->template->categoryInfo = $this->categorie;
        $this->template->products = $this->data->fetchAll();
    }

    //------HANDLE METHODS------------------------------------------------------
    public function handleDelete($id)
    {
        if (!$id) {
            $this->flashMessage('Kategorii se nepodařilo smazat.', 'alert alert-danger');
        } else {
            try {
                $this->category->delete($id);
                $this->flashMessage('Kategorie byla spěšně smazána.', 'alert alert-success');
            } catch (\Exception $ex) {
                $this->flashMessage('Kategorii se nepodařilo smazat.', 'alert alert-danger');
            }
        }
        if (!$this->isAjax()) {
            $this->redirect('this');
        }
    }

    public function handleDeleteProduct($id, $sortiment)
    {
        if (!$sortiment) {
            $this->flashMessage('Produkt se nepodařilo smazat.', 'alert alert-danger');
        } else {
            try {
                $this->sortiment->delete($sortiment);
                $this->flashMessage('Produkt byl úspěšně smazán.', 'alert alert-success');
            } catch (\Exception $ex) {
                $this->flashMessage('Produkt se nepodařilo smazat.', 'alert alert-danger');
            }
        }

        if (!$this->isAjax()) {
            $this->redirect('this', ['id' => $id]);
        }
    }

    public function handleUpgradeLevel($id)
    {
        try {
            $this->category->upgradeLevel($id);
            $this->flashMessage('Kategorie byla spěšně změněna.', 'alert alert-success');
        } catch (\Exception $ex) {
            $this->flashMessage('Kategorii se nepodařilo změnit.', 'alert alert-danger');
        }
        if (!$this->isAjax())
            $this->redirect('this');
    }

    //------COMPONENT METHODS---------------------------------------------------
    protected function createComponentAddCategory()
    {
        $form = $this->createForm();

        $form->onSuccess[] = array($this, 'formAddCategory');

        return ($form);
    }

    protected function createComponentUpdateCategory()
    {
        $form = $this->createForm($this->data);

        $form->onSuccess[] = array($this, 'formUpdateCategory');

        return ($form);
    }

    //------FORM SUCCESS METHODS------------------------------------------------
    public function formAddCategory(Form $form)
    {
        $values = $form->getValues();

        try {
            if ($values->parent_id == 0) {
                $values->parent_id = null;
            }
            $this->category->insert($values);
            $this->flashMessage("Kategorie byla úspěšně přidána", 'alert alert-success');
            $this->redirect("Category:list");
        } catch (\Nette\Database\UniqueConstraintViolationException $e) {
            $this->flashMessage("Kategorii se nepodařilo přidat", 'alert alert-danger');
        }
    }

    public function formUpdateCategory(Form $form)
    {
        $values = $form->getValues();

        try {
            if ($values->parent_id == 0) {
                $values->parent_id = null;
            }
            $this->category->update($this->id, $values);
            $this->flashMessage("Kategorie byla úspěšně změněna", 'alert alert-success');
            $this->redirect("Category:list");
        } catch (\Nette\Database\UniqueConstraintViolationException $e) {
            $this->flashMessage("Kategorii se nepodařilo změnit", 'alert alert-danger');
        }
    }

    //----- PRIVATE METHODS-----------------------------------------------------
    private function createForm($values = null)
    {
        $form = new Form;

        $name = $form->addText('name')
                ->setRequired('Prosím vyplňte název kategorie.');
        if ($values !== null) {
            $name->setDefaultValue($values['name']);
        }
        $select = $form->addSelect('parent_id', NULL, $this->categorie);
        if ($values !== null) {
            $select->setDefaultValue($values['parent_id']);
        }

        $form->addSubmit('send');

        $form->onError[] = array($this, 'formError');

        return ($form);
    }

    private function createSelect($prefix, $data)
    {
        foreach ($data as $key => $value) {
            if ($this->id === null || $this->id != $value['data']->category_id) {
                $this->categorie[$value['data']->category_id] = $prefix . $value['data']->name;
                if (count($value['subMenu'])) {
                    $this->createSelect($prefix . $value['data']->name . "->", $value['subMenu']);
                }
            }
        }
    }

}
