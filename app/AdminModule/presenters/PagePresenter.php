<?php

namespace App\AdminModule\Presenters;

use \Nette\Application\UI\Form as Form;

/**
 * Description of PagePresenter
 *
 * @author Lukáš Černý (xcerny63)
 */
class PagePresenter extends BasePresenter
{

    /** @var \App\Model\Page @inject */
    public $page;

    /** @var string */
    private $id;

    /** @var array */
    private $data = array();

    //----- ACTION METHODS------------------------------------------------------
    public function actionDefault()
    {
        $this->redirect('list');
    }

    public function actionList()
    {
        $this->data = $this->page->getAll()->fetchAll();
    }

    public function actionEdit($id)
    {
        if (!$id) {
            $this->flashMessage('V URL chybí ID.', 'alert alert-danger');
            $this->redirect('Employee:list');
        }
        $this->id = $id;

        $this->data = $this->page->getById($this->id)->fetch();

        if (!$this->data) {
            $this->flashMessage('Stránka nebyla nalezena.', 'alert alert-danger');
            $this->redirect('Page:list');
        }
    }

    public function renderList()
    {
        $this->template->list = $this->data;
    }

    //------HANDLE METHODS------------------------------------------------------
    //------COMPONENT METHODS---------------------------------------------------
    protected function createComponentEditPage()
    {
        $form = $this->createForm($this->data);

        $form->onSuccess[] = array($this, 'formEditPage');

        return ($form);
    }

    //------FORM SUCCESS METHODS------------------------------------------------
    public function formEditPage(Form $form)
    {
        $values = $form->getValues();

        try {
            $this->page->update($this->id, $values);
            $this->flashMessage("Stánka byla úspěšně upravena", 'alert alert-success');
            $this->redirect("Page:list");
        } catch (\Nette\Database\UniqueConstraintViolationException $e) {
            $this->flashMessage("Při úpravě nastala chyba", 'alert alert-danger');
        }
    }

    //----- PRIVATE METHODS-----------------------------------------------------
    private function createForm($values = null)
    {
        $form = new Form;

        $temp = $form->addText('title')
                ->setRequired('Zadejte prosím titulek.');
        if ($values !== null) {
            $temp->setDefaultValue($values['title']);
        }

        $temp = $form->addTextArea('content');
        if ($values !== null) {
            $temp->setDefaultValue($values['content']);
        }

        $form->addSubmit('send');

        $form->onError[] = array($this, 'formError');

        return ($form);
    }

}
