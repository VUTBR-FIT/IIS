<?php

namespace App\AdminModule\Presenters;

use \Nette\Application\UI\Form as Form;

/**
 * Description of ProductPresenter
 *
 * @author Lukáš Černý (xcerny63)
 */
class ProductPresenter extends BasePresenter
{

    /** @var \App\Model\Dodavatel @inject */
    public $transport;

    /** @var \App\Model\Kategorie @inject */
    public $category;

    /** @var \App\Model\Farbicky @inject */
    public $farbicky;

    /** @var \App\Model\Skicaky @inject */
    public $skicaky;

    /** @var \App\Model\Sortiment @inject */
    public $sortiment;

    /** @var \App\Model\DodavatelObjednavky @inject */
    public $order;

    /** @var integer */
    private $id;

    /** @var integer */
    private $categoryId;

    /** @var integer */
    private $type;

    /** @var array */
    private $data;

    /** @var array */
    private $arrTransport = array();

    //----- ACTION METHODS------------------------------------------------------
    public function actionDefault()
    {
        $this->redirect('detail');
    }

    public function actionUpdate($id, $categoryId)
    {
        if ($id === null || $categoryId === null) {
            $this->flashMessage('V URL chybí údaje.', 'alert alert-danger');
            if ($categoryId === null) {
                $this->redirect('Category:list');
            } else {
                $this->redirect('Category:detail', array('id' => $categoryId));
            }
        }

        $this->id = $id;
        $this->categoryId = $categoryId;

        $data = $this->transport->getAll();
        foreach ($data as $value) {
            $this->arrTransport[$value->dodavatel_id] = $value->nazov;
        }

        $this->data = $this->sortiment->getById($this->id)->fetch();

        if (!$this->data) {
            $this->flashMessage('Produkt nebyl nalezen.', 'alert alert-danger');
            $this->redirect('Category:detail', array('id' => $this->id));
        }

        $this->type = (\App\Model\Farbicky::isSortiment((array) $this->data)) ? 1 : 0;
    }

    public function actionAdd($id, $type)
    {
        if ($id === null || $type === null) {
            $this->flashMessage('V URL chybí údaje.', 'alert alert-danger');
            if ($id === null) {
                $this->redirect('Category:list');
            } else {
                $this->redirect('Category:detail', array('id' => $id));
            }
        }
        $this->id = $id;
        $this->type = ($type == 1) ? 1 : 0;

        if (!($this->category->getById($this->id)->fetch())) {
            $this->flashMessage('Kategorie nebyla nalezena.', 'alert alert-danger');
            $this->redirect('Category:list');
        }

        $data = $this->transport->getAll();
        foreach ($data as $value) {
            $this->arrTransport[$value->dodavatel_id] = $value->nazov;
        }
    }

    public function actionOrder($id, $categoryId)
    {
        if ($id === null || $categoryId === null) {
            $this->flashMessage('V URL chybí údaje.', 'alert alert-danger');
            if ($categoryId === null) {
                $this->redirect('Category:list');
            } else {
                $this->redirect('Category:detail', array('id' => $categoryId));
            }
        }

        $this->id = $id;
        $this->categoryId = $categoryId;

        $this->data[0] = $this->sortiment->getById($this->id)->fetch();
        if (!$this->data[0]) {
            $this->flashMessage('Produkt nebyl nalezen.', 'alert alert-danger');
            $this->redirect('Category:detail', array('id' => $this->id));
        }

        $this->data[1] = $this->transport->getById($this->data[0]->dodavatel_id)->fetch();
        if (!$this->data[1]) {
            $this->flashMessage('Dodavatel nebyl nalezen.', 'alert alert-danger');
            $this->redirect('Category:detail', array('id' => $this->id));
        }
        
        $data = $this->transport->getAll();
        foreach ($data as $value) {
            $this->arrTransport[$value->dodavatel_id] = $value->nazov;
        }
    }

    //----- RENDER METHODS------------------------------------------------------
    public function renderAdd()
    {
        $this->template->type = $this->type;
    }

    public function renderUpdate()
    {
        $this->template->type = $this->type;
    }

    public function renderOrder()
    {
        $this->template->sortiment = $this->data[0];
        $this->template->dodavatel = $this->data[1];
    }

    //------HANDLE METHODS------------------------------------------------------
    public function handleDelete($id, $sortiment)
    {
        if (!$sortiment) {
            $this->flashMessage('Produkt se nepodařilo smazat.', 'alert alert-danger');
        } else {
            try {
                $this->sortiment->delete($sortiment);
                $this->flashMessage('Produkt byl úspěšně smazán.', 'alert alert-success');
            } catch (\Exception $ex) {
                $this->flashMessage('Produkt se nepodařilo smazat.', 'alert alert-danger');
            }
        }

        if (!$this->isAjax()) {
            $this->redirect('this', $id);
        }
    }

    //------COMPONENT METHODS---------------------------------------------------
    protected function createComponentAddProduct()
    {
        $form = $this->createForm($this->type);

        $form->onSuccess[] = array($this, 'formAddProduct');

        return ($form);
    }

    protected function createComponentUpdateProduct()
    {
        $form = $this->createForm($this->type, $this->data);

        $form->onSuccess[] = array($this, 'formUpdateProduct');

        return ($form);
    }

    protected function createComponentOrderProduct()
    {
        $form = $this->createOrderForm();

        $form->onSuccess[] = array($this, 'formOrderProduct');

        return ($form);
    }

    //------FORM SUCCESS METHODS------------------------------------------------
    public function formAddProduct(Form $form)
    {
        $values = $form->getValues();

        try {
            $x = null;
            $values[\App\Model\Sortiment::CATEGORY_ID] = $this->id;
            if ($this->type == 1) {
                $x = $this->farbicky->insert($values);
            } else {
                $x = $this->skicaky->insert($values);
            }

            $this->flashMessage("Produkt byl úspěšně přidán.", 'alert alert-success');
            $this->redirect("Category:detail", array('id' => $this->id));
        } catch (\Nette\Database\UniqueConstraintViolationException $e) {
            $this->flashMessage("Produkt se nepodařilo přidat.", 'alert alert-danger');
        }
    }

    public function formUpdateProduct(Form $form)
    {
        $values = $form->getValues();

        try {
            $x = null;
            if ($this->type == 1) {
                $x = $this->farbicky->update($this->id, $values);
            } else {
                $x = $this->skicaky->update($this->id, $values);
            }

            $this->flashMessage("Produkt byl úspěšně upraven.", 'alert alert-success');
            $this->redirect("Category:detail", array('id' => $this->categoryId));
        } catch (\Nette\Database\UniqueConstraintViolationException $e) {
            $this->flashMessage("Produkt se nepodařilo přidat.", 'alert alert-danger');
        }
    }

    public function formOrderProduct(Form $form)
    {
        $values = $form->getValues();

        $values[\App\Model\DodavatelObjednavky::DODAVATEL] = $this->data[0]->dodavatel_id;
        $values[\App\Model\DodavatelObjednavky::SORTIMENT] = $this->id;
        $values[\App\Model\DodavatelObjednavky::SUM] = $this->data[0]->cena * $values['kusy'];
        
        try {
            $this->order->insert($values);
            $this->sortiment->update($this->id, [\App\Model\Sortiment::STATE => $values['datum_doruceni']]);
            $this->flashMessage("Objednávka byla odeslána.", 'alert alert-success');
            $this->redirect("Category:detail", array('id' => $this->categoryId));
        } catch (\Nette\Database\UniqueConstraintViolationException $e) {
            $this->flashMessage("Objednávka nebyla odeslána.", 'alert alert-danger');
        }
    }

    //----- PRIVATE METHODS-----------------------------------------------------
    private function createForm($type, $values = null)
    {
        $form = new Form;

        $temp = $form->addUpload('icon')
                ->setRequired(FALSE)
                ->addRule(Form::IMAGE, 'Obrázek musí být JPEG, PNG nebo GIF.');

        $temp = $form->addText('pocet')
                ->setRequired('Prosím zadejte počet dostupných kusů.')
                ->addRule(Form::PATTERN, "Prosím zadejte počet dostupných kusů.", "[0-9]+");
        if ($values !== null) {
            $temp->setDefaultValue($values['pocet']);
        } else {
            $temp->setDefaultValue(0);
        }

        $temp = $form->addText('typ')
                ->setRequired('Prosím zadejte typ.');
        if ($values !== null) {
            $temp->setDefaultValue($values['typ']);
        }

        $temp = $form->addText('nazov')
                ->setRequired('Prosím zadejte jmeno produktu');
        if ($values !== null) {
            $temp->setDefaultValue($values['nazov']);
        }

        $temp = $form->addText('dostupnost')
                ->setAttribute('type', 'date')
                ->setAttribute('placeholder', 'nevyplněno');
        if ($values !== null) {
            if ($values['dostupnost'] != null) {
                $temp->setDefaultValue($values['dostupnost']->format("Y-m-d"));
            }
        } else {
            $temp->setDefaultValue((new \Nette\Utils\DateTime())->format("Y-m-d"));
        }

        $temp = $form->addText('farba')
                ->setRequired('Zadejte prosím barvu.');
        if ($values !== null) {
            $temp->setDefaultValue($values['farba']);
        }

        $temp = $form->addText('cena')
                ->setAttribute('placeholder', '199.99Kč')
                ->setRequired("Prosím zadejte cenu")
                ->addRule(Form::PATTERN, "Zadejte prosím cenu.", "[0-9]+(\.[0-9]+|[0-9]*)");
        if ($values !== null) {
            $temp->setDefaultValue($values['cena']);
        }

        if ($type == 1) {
            // farbicky
            $temp = $form->addText('dlzka')
                    ->setRequired('Prosím zadejte délku tužky.');
            if ($values !== null) {
                $temp->setDefaultValue($values['dlzka']);
            }

            $temp = $form->addText('vlastnost')
                    ->setRequired('Prosím zadejte vlastnost tužky.');
            if ($values !== null) {
                $temp->setDefaultValue($values['vlastnost']);
            }
        } else {
            $temp = $form->addText('gramaz')
                    ->setRequired('Prosím zadejte gramáž papíru.');
            if ($values !== null) {
                $temp->setDefaultValue($values['gramaz']);
            }

            $temp = $form->addText('velikst')
                    ->setRequired('Prosím zadejte velikost skicáku.');
            if ($values !== null) {
                $temp->setDefaultValue($values['velikst']);
            }
        }

        $temp = $form->addSelect('dodavatel_id', null, $this->arrTransport)
                ->setRequired('Prosím vyberte dodavatele');
        if ($values !== null) {
            $temp->setDefaultValue($values['dodavatel_id']);
        }

        $form->addSubmit('send');

        $form->onError[] = array($this, 'formError');

        return ($form);
    }

    private function createOrderForm()
    {
        $form = new Form;

        $form->addText('kusy')
                ->setRequired("Zadejte prosím objednávaných kusů")
                ->setAttribute('type', 'number')
                ->addRule(Form::MIN, "Neplatná hodnota", 1);

        $form->addText('datum_doruceni')
                ->setRequired("Prosím zadejte datum doručení.")
                ->setAttribute('placeholder', "YYYY-MM-DD")
                ->setDefaultValue((new \Nette\Utils\DateTime())->format("Y-m-d"))
                ->setAttribute('type', 'date');
        
        $form->addSubmit('send');

        $form->onError[] = array($this, 'formError');

        return ($form);
    }

}
