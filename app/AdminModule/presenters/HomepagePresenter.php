<?php

namespace App\AdminModule\Presenters;

/**
 * Description of AdminHomePage
 *
 * @author Lukáš Černý (xcerny63)
 */
class HomepagePresenter extends BasePresenter
{

    /** @var \App\Model\Objednavka @inject */
    public $order;

    /** @var \App\Model\DodavatelObjednavky @inject */
    public $orderDelivery;

    /** @var \App\Model\Sortiment @inject */
    public $sortiment;

    /** @var Array */
    private $data;

    public function actionDefault()
    {
        $this->data[0] = $this->order->getAll(null)->fetchAll();
        $this->data[1] = $this->orderDelivery->getAll()->fetchAll();
    }

    public function renderDefault()
    {
        $this->template->customerOrders = $this->data[0];
        $this->template->deliveryOrders = $this->data[1];
    }

    public function handleChangeDeliveryStatus($id)
    {
        if ($id !== null) {
            $this->orderDelivery->update($id, [\App\Model\DodavatelObjednavky::STATE => 1]);
            $order = $this->orderDelivery->getById($id)->fetch();

            $this->sortiment->update($order->sortiment_id, [\App\Model\Sortiment::COUNT => $order->kusy]);

            if (!$this->isAjax())
                $this->redirect('this');
        }
    }

    public function handleUpdateState($id, $state)
    {
        $this->order->update($id, [\App\Model\Objednavka::STATE => $state]);

        if (!$this->isAjax())
            $this->redirect('this');
    }

}
