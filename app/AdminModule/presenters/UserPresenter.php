<?php

namespace App\AdminModule\Presenters;

use Nette\Application\UI\Form as Form;

/**
 * Description of UserPresenter
 *
 * @author user
 */
class UserPresenter extends BasePresenter
{

    /** @var \App\Model\Zamestnanec @inject */
    public $employee;

    /** @var \App\Model\User @inject */
    public $user;

    /** @var integer */
    private $id = null;

    /** @var array */
    private $data = array();

    public function actionUpdate()
    {
        $this->id = $this->getUser()->getId();

        $this->data = $this->employee->getById($this->id)->fetch();

        if (!$this->data) {
            $this->flashMessage('Uživatel nebyl nalezen.', 'alert alert-danger');
            $this->redirect('Homepage:');
        }
    }

    protected function createComponentUpdateEmployee()
    {
        $form = $this->createForm($this->data);

        $form->onSuccess[] = array($this, 'formUpdateEmployee');

        return ($form);
    }

    public function formUpdateEmployee(Form $form)
    {
        $values = $form->getValues();

        if ($values->password != $values->password_again) {
            $this->flashMessage("Hesla se neshodují", 'alert alert-warning');
        } else {
            try {
                if (strlen($values['password']) == 0) {
                    unset($values['password']);
                }
                $this->employee->update($this->id, $values);
                $this->flashMessage("Zaměstnanec byl úspěšně upraven", 'alert alert-success');
                $this->redirect("Homepage:");
            } catch (\Nette\Database\UniqueConstraintViolationException $e) {
                $this->flashMessage("V systému je tento mail již využíván", 'alert alert-danger');
            }
        }
    }

    private function createForm($values = null)
    {
        $form = new Form;

        $pass = $form->addText('password')
                ->setAttribute('type', 'password');
        if ($values !== null) {
            $pass->setAttribute('placeholder', 'nezměněno');
        } else {
            $pass->setRequired("Prosím zadejte vaše heslo");
        }

        $form->addText('password_again')
                ->setRequired(FALSE)
                ->setAttribute('type', 'password')
                ->addRule(Form::EQUAL, "Hesla se neshodují", $form['password']);

        $name = $form->addText('meno')
                ->setRequired("Prosím zadejte vaše jméno");
        if ($values !== null) {
            $name->setDefaultValue($values['meno']);
        }

        $surname = $form->addText('priezvisko')
                ->setRequired("Prosím zadejte vaše příjmení");
        if ($values !== null) {
            $surname->setDefaultValue($values['priezvisko']);
        }

        $birthNumber = $form->addText('rodne_cislo')
                ->setRequired("Prosím zadejte vaše rodné číslo")
                ->addRule(Form::PATTERN, "Zadaná hodnota neodpovídá rodnému číslu", '[0-9]{10}');
        if ($values !== null) {
            $birthNumber->setDefaultValue($values['rodne_cislo']);
        }

        $form->addSubmit('send');

        $form->onError[] = array($this, 'formError');

        return ($form);
    }

}
