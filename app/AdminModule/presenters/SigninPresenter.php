<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\AdminModule\Presenters;

/**
 * Description of AdminSignPresenter
 *
 * @author user
 */
class SigninPresenter extends BasePresenter
{

    /** @var \Nette\Security\User @inject */
    public $userModel;

    /** @var \App\Forms\FormFactory @inject */
    public $formFactory;

    protected function createComponentSignInForm()
    {
        $form = (new \App\Forms\SignInFormFactory($this->formFactory, $this->userModel))->create();
        $form->onError[] = array($this, 'formError');
        $form->onSuccess[] = array($this, 'signInSuccess');
        return ($form);
    }

    public function signInSuccess()
    {
        $this->flashMessage('Byl jste úspěšně příhlášen');
        $this->redirect('Homepage:');
    }

}
