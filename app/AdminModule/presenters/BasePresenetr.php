<?php

namespace App\AdminModule\Presenters;

/**
 * Description of AdminBasePresenetr
 *
 * @author user
 */
class BasePresenter extends \App\Presenters\BasePresenter
{

    protected function startup()
    {
        parent::startup();

        if (!$this->getUser()->isLoggedIn() && $this->name != 'Admin:Signin') {
            $this->redirect('Signin:default');
        } else if ($this->getUser()->isInRole(\App\Model\User::ROLE_CUSTOMER)) {
            $this->getUser()->logout();
            $this->redirect('Signin:default');
        }
    }

    public function beforeRender()
    {
        parent::beforeRender();
        $this->setLayout('layoutAdmin');
    }
    
    /**
     * @Overrides method
     * @param \Nette\Application\UI\Form $form
     */
    public function formError(\Nette\Application\UI\Form $form)
    {
        foreach ($form->getErrors() as $error) {
            $this->flashMessage($error, 'alert alert-warning');
        }
    }

    public function handleLogout()
    {
        $this->getUser()->logout(true);
        $this->redirect('this');
    }

}
