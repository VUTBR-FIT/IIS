<?php

namespace App\Presenters;

use Nette;
use App\Forms;

/**
 * @autor Lukáš Černý (xcerny63)
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    protected $user = null;
    
    
    public function beforeRender()
    {
        parent::beforeRender();
        $this->user = $this->getUser();
        if ($this->user != null) {
            $this->template->user = $this->user;
        } else {
            $this->user = new \Nette\Security\User();
            $this->template->user = $this->user;
        }
    }
    
    
    public function formError(\Nette\Application\UI\Form $form)
    {
        foreach ($form->getErrors() as $error) {
            $this->flashMessage($error, 'error');
        }
    }

}
