## Autoři:
 - Blašková Adriana (frontend)
 - Černý Lukáš (backend)

## Instalce
``` bash
   $> composer install
```
